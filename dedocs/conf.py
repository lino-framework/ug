# -*- coding: utf-8 -*-
from pathlib import Path
docs = Path().absolute().parent / 'docs'
# docs = Path('../docs').resolve()
fn = docs / 'conf.py'
with open(fn, "rb") as fd:
    exec(compile(fd.read(), fn, 'exec'))

# html_static_path = [str(docs / '.static')]
# templates_path = [str(docs / '.templates')]

language = "de"

project = "Lino Benutzerhandbuch"
html_title = "Lino Benutzerhandbuch"


rst_prolog = """

:doc:`Grundkurs </basics/index>` |
:doc:`Anwendungen </apps/index>`

"""
