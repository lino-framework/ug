==========================
Lino Così Benutzerhandbuch
==========================

Lino Così ist ein einfaches Buchhaltungsprogramm. Es ist einfach, aber kann mit
Ihren wachsenden Bedürfnissen folgen. Die gleichen Begriffe und Funktionen
werden auch in komplexeren Lino-Anwendungen wie :ref:`tera` oder :ref:`voga`
benutzt.

Wir sprechen den Namen italienisch aus (etwa so wie ein deutschsprachiger Leser
"kosie" lesen würde), er entspringt einerseits zwei Silben unserer anfänglichen
Frage "Comment faire une **co**\ mptabilité **si**\ mple?" (französisch für "Wie
macht man eine einfache Buchhaltung?") und ist andererseits die Antwort darauf:
"Sosì!" (Italienisch fur "So!").

.. toctree::
   :maxdepth: 2

   tour/index
