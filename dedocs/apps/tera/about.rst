==============
Über Lino Tera
==============

Lino Tera ist ein Verwaltungsprogramm für Therapiezentren.

- Website: https://tera.lino-framework.org

- Benutzerhandbuch
  https://using.lino-framework.org/de/apps/tera

- Technische Dokumentation
  https://www.lino-framework.org/specs/tera

- Hosting, Support und Wartung
  https://www.saffre-rumma.net
