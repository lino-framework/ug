======================================
Lino als Schnittstelle zum Peppol-Netz
======================================

.. contents::
    :depth: 1
    :local:


Einleitung
==========

Ab Januar 2026 müssen alle belgischen Unternehmen ihre Rechnungen über das
Peppol-Netzwerk verschicken.  Die neue Regelung ist für kleine Entwickler von
Buchhaltungssoftware eine seriöse Herausforderung. Was bedeutet das für
TIM-Benutzer?

Rumma & Ko arbeitet seit Sommer 2024 an einer Lösung.  Bis
September 2025 wollen wir alle TIM-Kunden bereit für Peppol haben.

Hier unser allgemeiner Fahrplan:

- Sie arbeiten weiter mit TIM so lange Sie wollen. Peppol bedeutet nicht das
  Ende von TIM.

- Für jeden TIM-Kunden wird eine Lino-Anlage eingerichtet, die als Schnittstelle
  zum Peppol-Netz fungiert. Die Daten aus Ihrem TIM werden täglich automatisch
  an Ihren Lino geschickt.

- In Ihrem Lino verwalten Sie die Kommunikation mit dem Peppol-Netzwerk.

- Nebenbei können Sie mit eigenen Augen und an den eigenen Daten sehen, wie Ihr
  TIM aussähe, wenn es ein Lino wäre. Und vielleicht wollen Sie dann irgendwann
  sogar umsteigen. Aber niemand zwingt oder drängt Sie.

- Alle Schritte werden individuell geplant.

Checkliste
==========

- Sie bestätigen Rumma & Ko, dass Sie mitmachen wollen. Wir machen Ihnen ein
  Angebot über Ihre Beteiligung an den Entwicklungskosten, für die der Staat
  auch `zwei steuerliche Unterstützungsmaßnahmen für KMU und Selbständige
  <https://erechnung.belgium.be/de/article/strukturierte-elektronische-rechnungen-zwischen-unternehmen-werden-ab-2026-verpflichtend>`__
  vorgesehen hat.

- Rumma & Ko richtet Ihnen einen Lino-Server ein.

- Gemeinsam installieren wir auf Ihrem TIM-Rechner ein Programm, das die
  TIM-Daten täglich zu Ihrem Lino kopiert.

- Sie erhalten von Rumma & Ko ein Passwort, um sich auf Ihrem Lino einzuloggen.

- Rumma & Ko gibt Ihnen Anleitung zur Benutzung der Peppol-Schnittstelle.
  Sie machen sich damit vertraut.

- Sie aktivieren Peppol für Ihr Unternehmen. Dadurch sehen Ihre
  Geschäftspartner, dass Sie auf Peppol umgestiegen sind und Ihre Rechnungen
  elektronisch verschicken und empfangen können.

Externe Links
=============

- https://erechnung.belgium.be/de

- https://hermes-belgium.be/hermeslogin?lang=fr

- https://dev.lino-framework.org/topics/peppol.html
