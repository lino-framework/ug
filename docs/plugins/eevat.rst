.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.eevat:

==============================================
The ``eevat`` plugin
==============================================

Estonian VAT declaration

.. currentmodule:: lino_xl.lib.eevat

See :mod:`lino_xl.lib.eevat`.


.. contents::
   :depth: 1
   :local:
