.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.households:

======================================================
The ``households`` plugin
======================================================

The :mod:`lino_xl.lib.households` plugin adds the notions of :term:`households
<household>`, i.e. groups of humans who live together in a same house. A person
can leave a household and join another one. You can address a household as a
:term:`partner`.


.. contents::
  :local:

Concepts
=========

.. glossary::

  household

    A group of :term:`persons <person>` who live together in a same house.

  household membership

    The fact that a given :term:`person` is (or has been) part of a given
    :term:`household`.

  household type

    The type of a :term:`household`. For example "family", "shared appartment",
    "married couple", "factual household", "legal cohabitation", etc.

The list of :term:`household types <household type>` is a :term:`choicelist`. It
can vary depending on the purpose of your :term:`Lino site` and the laws in your
country (example: `Ménage de fait
<http://www.belgium.be/fr/famille/couple/cohabitation/>`__).
