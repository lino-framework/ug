.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.changes:

==============================================
The ``changes`` plugin
==============================================

.. currentmodule:: lino_xl.lib.changes

History of changes in database.

See :mod:`lino.modlib.changes`.


.. contents::
   :depth: 1
   :local:
