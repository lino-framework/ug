.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.boards:

==============================================
The ``boards`` plugin
==============================================

The ``boards`` pluginis for managing boards of people making decisions.

.. currentmodule:: lino_xl.lib.boards

See :mod:`lino_xl.lib.boards`.


.. contents::
   :depth: 1
   :local:
