.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.bevats:

===============================================
The ``bevats`` plugin
===============================================

.. currentmodule:: lino_xl.lib.bevats

Simplified Belgian VAT declaration.

See :mod:`lino_xl.lib.bevats`.


.. contents::
   :depth: 1
   :local:
