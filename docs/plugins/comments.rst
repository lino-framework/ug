.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.comments:

====================================
The ``comments`` plugin
====================================

The ``comments`` plugin adds functionality for letting site users discuss by
writing comments.

.. contents::
   :depth: 1
   :local:


Overview
========

The :term:`detail window` of a :term:`commentable` database row usually has two
widgets for commenting:

- A :guilabel:`Comments` panel, which shows all comments.  You can click on
  the |insert| button in order to add your comment.

- A :guilabel:`My comment` field. You type your comment here and hit
  :kbd:`Ctrl+S` or click the |save| button to submit it. This is just a shortcut
  for the |insert| button of the :guilabel:`Comments` panel.

In the :guilabel:`Comments` panel you can click on the time indication of a
comment in order to open the detail view of that particular comment. In this
view you can

- Edit the comment (if you have permission to do so, which is
  application-specific).

- reply to that comment to start a :term:`discussion thread` (which is more
  precise that just commenting on the :term:`discussion topic`.

- Pick an emotion. Emotions are a kind of minimlistic voting system about a
  comment. Picking an emotion expresses your opinion about that comment. You can
  change your opinion any time, but you can have only one opinion per comment.

  The :term:`server administrator` can edit the list of available emotion icons.

Visibility of comments
======================

Lino leaves it up to the :term:`application <Lino application>` to define
rules for regulating who can see a comment.

- The author of a comment can mark it as **confidential**.
- Lino asks the :term:`commentable` who is part of the :term:`discussion group`

- The comments plugin provides different :term:`user roles <user role>` for
  managing permissions about comments.


Menu commands
=============

This plugin also adds the following menu commands:

- :menuselection:`Office --> My comments`
- :menuselection:`Office --> Recent comments`
- :menuselection:`Explorer --> Office --> Comments`


Glossary
========

.. glossary::

  comment

    A written text that one user wants to share with others.

    A comment is always **about** a given :term:`discussion topic`.
    All comments about a given topic are called a :term:`discussion`.

    A comment has no "recipient". When you submit a comment, Lino notifies the
    other members of the :term:`discussion group`

    A comment can be a *reply* to another comment. All comments replying
    directly or indirectly to a given comment are called a :term:`thread
    <discussion thread>`.

    Comments are stored in the :class:`Comment` database model.

  discussion

    All comments about a given topic

  discussion topic

    A :term:`database row` that serves as the *topic* of a series of comments.

    The :term:`discussion topic` is stored in the :attr:`owner` field of the
    comment (which is defined by the :class:`Controllable` mixin).

  discussion thread

    The comments that reply directly or indirectly to a given :term:`comment`.

  discussion group

    The group of :term:`site users <site user>` who participate in a discussion.

    Lino leaves it up to the :term:`application <Lino application>` to define
    rules for this. For example in :ref:`noi` the *team* of a :term:`ticket`
    defines the discussion group, in :ref:`welfare` all coaches of a client, in
    :ref:`chatter <book.projects.chatter>` the members of the :term:`user
    group`, in :ref:`avanti` nobody gets notified.

  commentable

    A :term:`database row` that can potentially become :term:`discussion topic`.

    Lino leaves it up to the :term:`application <Lino application>` to define
    which :term:`database models <database model>` are :term:`commentable`. For
    example in :ref:`noi` users can discuss about :term:`tickets <ticket>`, in
    :ref:`avanti` about clients, in :ref:`amici` about :term:`persons <person>`
    and :term:`organizations <organization>`.


See also
========

- Developer documentation: :ref:`dg.plugins.comments`
