.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.sources:

============================================
The ``sources`` plugin
============================================

The :mod:`lino_xl.lib.sources` plugin adds the notions for managing your
:term:`bibliographic sources <bibliographic source>`, including lists of
sources, authors and licenses.


.. contents::
  :local:

Overview
========


.. glossary::

  bibliographic source

    A database entry that holds information about some published content that
    you may want to refer to.
