.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.working:

================================
The ``working`` plugin
================================

.. currentmodule:: lino_xl.lib.working

The :mod:`lino_xl.lib.working` plugin adds functionality for registering work
time using :term:`working sessions <working session>`.

.. contents::
   :depth: 1
   :local:


Overview
========

.. glossary::

  working session

    A database record expressing the fact that a given user works (or has been
    working) on a given "ticket" for a given lapse of time.

    The meaning of "ticket" is given by the :setting:`working.ticket_model`
    plugin setting.

  service report

    A document that reports the working sessions that have been done for a given
    customer during a given period.

Sessions
========

Extreme case example of a :term:`working session`:

- I start to work on an existing ticket #1 at 9:23.  A customer phones
  at 10:17 with a question. I create #2 for this.  That call is interrupted
  several times by the customer himself.  During the first
  interruption another customer calls, with another problem (ticket
  #3) which we solve together within 5 minutes.  During the second
  interruption of #2 (which lasts 7 minutes) I make a coffee break.

  During the third interruption I continue to analyze the
  customer's problem.  When ticket #2 is solved, I decided that
  it's not worth to keep track of each interruption and that the
  overall session time for this ticket can be estimated to 0:40.

  ::

    Ticket Start End    Pause  Duration
    #1      9:23 13:12  0:45
    #2     10:17 11:12  0:12       0:43
    #3     10:23 10:28             0:05


Reporting type
==============

The :attr:`reporting_type <Session.reporting_type>` of a working session
indicates how the client is going to pay for the work done.


Worked hours
============

The :class:`WorkedHours` table is useful to manually edit your working times or
to see on which tickets you have been working recently. It is shown in your
dashboard (unless you configured your dashboard to hide it).

.. class:: WorkedHours

  Shows a summary of your :term:`working sessions <working session>` of the last
  seven days, one row per day.

To manually edit your :term:`working sessions <working session>`, click on a date in
the `Description` column to open :class:`MySessionsByDate`.

In the :guilabel:`Worked tickets` column you see a list of the tickets on which
you worked that day. This is a convenient way to continue some work you started
some days ago.
