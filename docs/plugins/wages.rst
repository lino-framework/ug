.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.wages:

==============================================
The ``wages`` plugin
==============================================

.. currentmodule:: lino_xl.lib.wages

Payrolls for wages and salaries.

See :mod:`lino_xl.lib.wages`.


.. contents::
   :depth: 1
   :local:
