.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.shopping:

==============================================
The ``shopping`` plugin
==============================================

Adds :term:`database models <database model>` for the :term:`shopping cart`,
:term:`delivery methods <delivery method>` and the :term:`shopping addresses
<shopping address>` of a user.

For technical details see the :mod:`lino_xl.lib.shopping` plugin.

.. currentmodule:: lino_xl.lib.shopping


.. contents::
   :depth: 1
   :local:


Glossary
========

.. glossary::

  shopping cart

    A :term:`database row` representing the fact that a given user plans to buy
    a set of products.  A shopping cart has one item per product for which the
    users has clicked "Add to cart". It also has an action to create an invoice.

  shopping address

    A postal address that can be used either as invoicing address or delivery
    address.

    Shopping addresses are not stored using :doc:`contacts plugin <contacts>`
    because they are private to each user and won't be used as business partners
    of the :term:`site operator`.

  delivery method

    A method to be used for delivery of a :term:`shopping cart`.

    The :term:`end user` must set this before they can start an order (invoice)
    from their :term:`shopping cart`.

    The list of available delivery methods is to be maintained by the
    :term:`site manager`.
