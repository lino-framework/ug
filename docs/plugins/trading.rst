.. _ug.plugins.trading:

==============================
The ``trading`` plugin
==============================

.. currentmodule:: lino_xl.lib.trading

The :mod:`lino_xl.lib.trading` plugin adds functionality for managing sale,
purchase or other kind of trading activity with named things (e.g. "products",
"services").

Table of contents:

.. contents::
   :depth: 1
   :local:


Concepts
========

.. glossary::

  trade voucher

    A :term:`ledger voucher` that has a :term:`partner` as recipient and one or
    several "rows" ("items"), each of which can refer to a :term:`product`.

    For example, :term:`product invoices <product invoice>` and :term:`service
    reports <service report>` are trade vouchers.

  product invoice

    An legal document that describes that something (a series of invoice items)
    has been sold to a given :term:`customer` or purchased from a given
    :term:`provider`. The :term:`business partner` can be either a private
    person or an organization.

  customer

    The :term:`business partner` of a sales trading transaction.

  provider

    The :term:`business partner` of a purchase trading transaction.

Dependency
==========

This plugin requires the
:doc:`products <products>`
:doc:`contacts <contacts>`
and the :doc:`accounting <accounting>` plugin.
