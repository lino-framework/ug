.. doctest docs/plugins/cal.rst
.. _ug.plugins.cal:
.. _ug.plugins.calview:

=============================
The ``cal`` plugin
=============================

.. currentmodule:: lino_xl.lib.cal

The calendar module is implemented by the
:mod:`lino_xl.lib.cal` and :mod:`lino_xl.lib.calview` plugins.
This document is about general calendar functionality.
:doc:`/topics/cal_auto` are handled in a separate article.

Lino isn't very much used as a real-life calendar application because --you'll
have guessed why-- there are powerful competing products on the market. We are
looking for users who nevertheless want to give it a serious try!


.. contents::
  :local:

Calendar entries
================

.. glossary::

  calendar entry

    A lapse of time to be visualized in a :term:`calendar view`.

A calendar entry is stored in the database as an instance of the :class:`Event`
:term:`database model`.

A calendar entry usually has a given :term:`type <calendar entry type>`.

It can have a list of guests or participants (more precisely :term:`presences
<presence>`).


Lifecycle of a calendar entry
=============================

Every :term:`calendar entry` has a given **state**, which can change according
to rules defined by the application.

Common calendar entry states are "suggested", "took place", "cancelled", ...


Calendar entry types
====================

The list of :term:`calendar entry types <calendar entry type>` is configurable
via :menuselection:`Configure --> Calendar --> Calendar entry types`.

Often used calendar entry types are :term:`appointment`, informal meeting,
public event, holiday.

The type of a calendar entry can be used to change behaviour and rules.


Calendars
=========

Calendar entries can be grouped into "calendars".


Conflicting calendar entries
============================

When two calendar entries conflict with each other, Lino shows this as a
:term:`data problem message`.

Click on the :guilabel:`Check data` button to run this test explicitly.
Otherwise it is run periodically, e.g. every evening, depending on how your
Lino site is configured.


Transparent calendar entries
============================

.. glossary::

  transparent calendar entry

    A calendar entry that does never conflict with other entries.

For example the entry type "Internal" is marked "transparent".


The guests of a calendar entry
==============================

A calendar entry can have a list of **guests**. A guest is when a given person
is *expected to attend* or *has been present* at a given calendar entry.
Depending on the context the guests of a calendar entry may be labelled
"guests", "participants", "presences", ...

Every participant of a calendar entry can have a "role". For example
in a class meeting you might want to differentiate between the teacher
and the pupils.


Remote calendars
================

A **remote calendar** is a set of calendar entries stored on another server.
Lino periodically synchronized the local data from the remote server, and local
modifications will be sent back to the remote calendar.

The feature is not currently being used anywhere.

Rooms
=====

A **room** is location where :term:`appointments <appointment>` can happen.  For
a given room you can see the calendar entries that happened (or will happen)
there.  A room has a multilingual name that can be used in :term:`printable
documents <printable document>`.

Applications might change the term "Room" into something else if the application
is not interested in physical rooms. For example :ref:`presto` calls them
"Team".

Subscriptions
=============

A **subscription** is when a :term:`site user` is interested in a given calendar.


Tasks
=====

A task is when a user plans to do something (and optionally wants to get
reminded about it).


Glossary
=========


.. glossary::

  calendar view

    A specialized :term:`data window` that is optimized for showing calendar
    entries and in monthly, weekly or daily display mode.

  calendar entry state

    The :term:`workflow state` of a :term:`calendar entry`.

    See `Lifecycle of a calendar entry`_ below.


  calendar entry type

    The type of a :term:`calendar entry`.

    See `Calendar entry types`_.

  appointment

    A :term:`calendar entry` for which other people (external partners or
    colleagues) are involved and should be informed about schedule changes.

    French: Rendez-vous, German: Termin.

    Appointments are defined by assigning them a :term:`calendar entry type` that has
    the :attr:`EventType.is_appointment` field checked.

  all-day

    An **all-day** calendar entry is one that has no starting and ending time.

  same-day

    A **same-day** calendar entry is one that ends on the same date as it
    starts. The ending date field is empty. If a somae-day calendar entry  has
    its **ending time before the starting time**, the ending date is understood
    as the day after the starting date.

  presence

    The fact that a given person is an expected participant to a given
    :term:`calendar entry`.

  task

    Something a user plans to do.

  daily planner

    A table showing a calendar overview for a given day.  Both the rows and the
    columns can be configured per application or locally per site.

    See :class:`DailySlave`.

  My calendar entries

    A table showing today's and all future appointments of the user who requests
    it. It starts today and is sorted ascending.
