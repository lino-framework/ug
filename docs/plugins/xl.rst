.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.xl:

==============================================
The ``xl`` plugin
==============================================

The ``xl`` plugin is main plugin of the Extensions Library.

See also :ref:`dev.xl`.


.. currentmodule:: lino_xl.lib.xl

See :mod:`lino_xl.lib.xl`.


.. contents::
   :depth: 1
   :local:
