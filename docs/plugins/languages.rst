.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.languages:

==============================================
the ``languages`` plugin
==============================================

.. currentmodule:: lino_xl.lib.languages

Adds a list of languages.

See :mod:`lino_xl.lib.languages`.


.. contents::
   :depth: 1
   :local:
