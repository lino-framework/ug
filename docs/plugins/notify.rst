.. _ug.plugins.notify:

=========================
The ``notify`` plugin
=========================

This plugin adds functionality for managing :term:`notification messages
<notification message>`. Lino shows notifications on your :term:`dashboard`,
sends them as `Email notifications`_ and/or pushes them as `Desktop
notifications`_.

.. currentmodule:: lino.modlib.notify

.. contents::
   :depth: 1
   :local:

Overview
========

.. glossary::

  notification message

    A message sent by a :term:`Lino site` to a user of the site.

  broadcast notification

    A :term:`notification message` sent to all users of a site.

  notification message type

    The type of a :term:`notification message`.

    A Lino application defines a list of notification message types, and a
    :term:`site manager` can mask certain notification message types for
    certain :term:`user types <user type>` in order to filter out notifications
    of this type for users of this type.

Similar functionalities
=======================

Don't mix up notifications with **comments** (implemented by
:mod:`lino.modlib.comments`). Comments are similar to notifications in that they
describe some "event" that happened to some :term:`database object`, but unlike
notifications, comments are never addressed to a single recipient. And while
notifications are to be delivered to their recipients as quickly as possible,
comments are rather long-term historic entries, visible to every user who has
the required permission.

Don't mix up notifications with **system notes** (implemented by
:mod:`lino_xl.lib.notes`), similar to comments.

Unlike comments and system notes, the history of notifications is stored only
for a limited time, usually a few weeks. Also the sender of a notification
message is always the site itself, not a user. That is, you cannot ask Lino to
show you a "list of all notification messages emitted by a given user".


Email notifications
===================

Every :term:`site user` can specify in their :term:`user settings` whether
and how often they want to get notified via email.


Desktop notifications
=====================

.. glossary::

  desktop notifications

    A feature that displays instant messages issued by a :term:`Lino site` to
    the display of a :term:`client device`, potentially with an acoustic or
    vibration signal.

How to enable :term:`desktop notifications` on your device:

- The :term:`Lino site` must have been configured to send notifications.  And
  the Lino application must use the notify plugin. And if the application uses
  the plugin, the :term:`server administrator` may still have disabled desktop
  notifications.

- You must open a browser window and sign in to the :term:`Lino site`.

- Your browser must ask your permission to show desktop notifications from that
  website, and you must answer "Yes".


Marking notifications as seen
=============================

Independently of whether notifications have been delivered via email and as
desktop notification, Lino displays unseen notification messages in the
:term:`dashboard` where it also provides an action for marking individual
message as seen.

A common caveat is that Lino does not know whether you saw the desktop
notification or the email.  That's why all notifications remain on your
dashboard until you tick them off explicitly.

- It can be disturbing to read a message again in the dashboard if you
  have just read by email or as a desktop notification.

- Some users tend to not care about marking their notifications as
  seen in the dashboard, which causes their "My notification messages"
  to become overfilled and useless.

- Don't misunderstand the My notifications widget as a to-do
  list, which is not a good idea.

There is no perfect solution for these problems.  One workaround is to instruct
Lino to also delete *unseen* notifications automatically, by setting
:data:`keep_unseen` to `False`.  Here is an example which also increases
:data:`remove_after` to 180 days (6 months)::

   SITE.plugins.notify.configure(remove_after=180, keep_unseen=False)

Users can hide the `MyMessages` widget in their :term:`user settings` if
:mod:`lino.modlib.dashboard` is installed as well.  But that's not a
recommended solution.  If you see that users of your application are
doing this, you should analyze why they do it and e.g. add filtering
options.

Possible optimizations of the system:

- Marking notifications as seen in the dashboard can be a bit slow
  because Lino refreshes the whole dashboard after every click.  We
  could avoid this using javascript which sets the item to hidden
  instead of calling refresh.

- Add a &notify=123456" (the id of the message) to every link in the
  email so that when the user follows one of them, the message can get
  marked as seen.


.. _ug.notify.Message.owner:

The owner of a message
======================

Every :term:`notification message` usually has an :attr:`owner <Message.owner>`.
The owner is another :term:`database object` that represents the "topic" of this
message. It expresses what this message is about.

Lino can optionally make that a given :term:`site user` will receive only the
first notification message about a given owner.  Any following message is
ignored until the site user has marker the first message as seen. Typical use
case are the messages emitted by :class:`ChangeNotifier`: you don't want to get
10 mails just because a colleague makes 10 small modifications when authoring
the text field of an observed database object.
