.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.beid:

==================================
The ``beid`` plugin
==================================

.. currentmodule:: lino_xl.lib.beid

The ``beid`` plugin adds functionality for reading Belgian ID cards of your
business partners.

See :mod:`lino_xl.lib.beid`.


.. contents::
   :depth: 1
   :local:
