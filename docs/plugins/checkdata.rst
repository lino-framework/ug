.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.checkdata:

==============================================
The ``checkdata`` plugin
==============================================

The :mod:`lino.modlib.checkdata` plugin adds support for defining
application-level data integrity tests.

.. currentmodule:: lino.modlib.checkdata


.. contents::
   :depth: 1
   :local:

Overview
========

This plugin provides a framework for managing and handling :term:`data problems
<data problem>`.

.. glossary::

  data problem

    A problem in the database that cannot be detected by the DBMS because
    finding it requires business intelligence.

    Some data problems can be fixed automatically, others need human
    interaction.


The application developer defines the rules for detecting data problems by
writing :term:`data checkers <data checker>`.

.. glossary::

  data checker

    A piece of code that tests for :term:`data problems <data problem>`.

    Also known as (soft) "integrity test".

  unbound data checker

    Data checkers are usually attached to a given database model. If they are
    not attached to a model, they are called **unbound**.  :term:`Data problem messages
    <data problem>` reported by an unbound data checker have an empty
    :attr:`owner <Problem.owner>` field.

Lino has different ways to run these checkers:

- A button with the bell |bell| ("Check data") in the toolbar of a database row
- The :cmd:`pm checkdata` command is run daily on a production site.



When a data checker finds a problem, Lino creates a :term:`data problem
message`.

.. glossary::

  data problem message

    A message that describes one or several :term:`data problems <data problem>`
    detected in a given :term:`database object`.

Data problem messages are themselves database objects, but considered temporary
data and may be updated automatically without user confirmation.

Each :term:`data problem message` is assigned to a *responsible user*. This user
can see their problems in the :class:`MyMessages` table, which generates a
welcome message if it contains data.
