.. _ug.plugins.bevat:

==================================
The ``bevat`` plugin
==================================

Belgian VAT declarations

.. currentmodule:: lino_xl.lib.bevat

The :mod:`lino_xl.lib.bevat` plugin is the :term:`national VAT module`
for a standard Belgian subjectible :term:`site operator`.

.. contents::
   :depth: 1
   :local:

Overview
========


For a :term:`site operator` located in Belgium, the :term:`tax office` is the
Finance Department of the Federal Public Service. They maintain a `List of XML
specifications
<https://finances.belgium.be/fr/E-services/Intervat/documentation-technique>`__.
The following XML statements from this list  are implemented (or planned) in
Lino:

- Déclaration périodiques (:term:`VAT declaration` for bevat):
  NewTVA-in_v0_9.xsd

- Déclaration spéciale (:term:`VAT declaration` for bevats):
  VAT629-in_v0_9.xsd

- Relevé Intracommunautaire (:term:`intra-community statement`)
  be: NEWICO-in_v0_9.xsd
  https://www.emta.ee/eng/business-client/income-expenses-supply-profit/submission-report-intra-community-supply

- Liste clients (:term:`annual list of domestic customers`):
  NewLK-in_v0_9.xsd



External references
===================

-  `finances.belgium.be | Entreprises | TVA  <https://finances.belgium.be/fr/entreprises/tva>`__

- How to submit a :term:`VAT declaration` :
  `en <https://finance.belgium.be/en/E-services/Intervat/how-to-use-intervat/submit-periodic-return>`__
  `fr <https://finances.belgium.be/fr/entreprises/tva/declaration/declaration-periodique>`__

- How to submit an :term:`intra-community statement` :
  `en <https://finance.belgium.be/en/E-services/Intervat/how-to-use-intervat/submit-intra-community-statement#q4>`__
  `fr <https://finances.belgium.be/fr/entreprises/tva/declaration/releve_intra_communautaire>`__

- How to submit the :term:`annual list of domestic customers`:

  - `Entreprises | TVA | Déclaration | Liste annuelle des clients assujettis <https://finances.belgium.be/fr/entreprises/tva/declaration/liste_annuelle_clients_assujettis>`__

  - `E-services | Intervat | Comment utiliser Intervat ? | Déposer un listing clients (ou liste annuelle des clients assujettis) via Intervat <https://finances.belgium.be/fr/E-services/Intervat/comment-utiliser-intervat/deposer-listing-clients>`__
