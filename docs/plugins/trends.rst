.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.trends:

==============================================
The ``trends`` plugin
==============================================

.. currentmodule:: lino_xl.lib.trends

Observing the evolution of people.

See :mod:`lino_xl.lib.trends`.


.. contents::
   :depth: 1
   :local:
