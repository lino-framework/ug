.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.dupable:

==============================================
The ``dupable`` plugin
==============================================

Finding duplicate records.

.. currentmodule:: lino_xl.lib.dupable

See :mod:`lino_xl.lib.dupable`.


.. contents::
   :depth: 1
   :local:
