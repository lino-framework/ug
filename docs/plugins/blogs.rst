.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.blogs:

==================================
The ``blogs`` plugin
==================================

.. currentmodule:: lino_xl.lib.blogs

The ``blogs`` plugin adds functionality for managing your blogs.

.. contents::
   :depth: 1
   :local:

.. glossary::

    blog entry

      A blog entry is a short article with a title, published on a given
      date and time by a given user.


    blog

      A series of blog entries.
