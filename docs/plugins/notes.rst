.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.notes:

==================================
The ``notes`` plugin
==================================

.. currentmodule:: lino_xl.lib.notes

The ``notes`` plugin adds functionality for writing "notes".
It is deprecated in favour of the :doc:`comments` plugin.

See :mod:`lino_xl.lib.notes`.


.. contents::
   :depth: 1
   :local:
