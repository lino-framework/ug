.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.rooms:

============================================
The ``rooms`` plugin
============================================

.. currentmodule:: lino_xl.lib.rooms

See :mod:`lino_xl.lib.rooms`.


.. contents::
   :depth: 1
   :local:
