.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.phones:

=================================================
The ``phones`` plugin
=================================================

.. currentmodule:: lino_xl.lib.phones

Multiple phone numbers, email addresses and other contact details per partner.

See :mod:`lino_xl.lib.phones`.


.. contents::
   :depth: 1
   :local:
