.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.uploads:

===============================================
The ``uploads`` plugin
===============================================

.. currentmodule:: lino_xl.lib.uploads

The :mod:`lino_xl.lib.uploads` plugin adds functionality for uploading files to
the server and managing them. This includes the upload action as such, but also
processing the uploaded files, such as linking them to other database content or
monitoring their validity.

.. contents::
   :depth: 1
   :local:


Overview
========

The plugin differentiate between **web uploads** and **library uploads**. They
differ by the way they arrived to the server. Web uploads were uploaded by some
:term:`end user` via the web interface, while *library uploads* have arrived to
the server by some other means.  Web uploads are stored below the :term:`uploads
folder` while library uploads are stored below the folder given by their
:term:`library volume`.

A third type of upload files are **fileless uploads**. These are upload files
without an actual file. A fileless upload represents the fact that some external
document exists, but just hasn't been uploaded. There are people who are
interested in this kind of fact ;-)

All :term:`upload entries <upload entry>` are stored in a single database table
called :class:`Upload`.

An :term:`upload file` is usually linked or associated to another
:term:`database object`, called its :term:`controller <upload controller>`,
which represents its "reason of being".

The site manager can configure :term:`library volumes <library volume>` via the
menu command :menuselection:`Configure --> Uploads --> Library volumes`.  Lino
watches the files in the folder tree of a library volume and can automatically
create an :term:`upload file` for every new file.

Concepts
========

.. glossary::

  upload entry

    A database record that represents an independent media file that has been
    uploaded to the :term:`Lino site` either via the web interface or as a
    library file.

  upload file

    An :term:`upload entry` that represents a file.

  uploads folder

    The folder below which all uploaded files are being stored on the server.

  upload type

    The type of an upload file.

  upload area

    A group of :term:`upload types <upload type>` that are being displayed in a
    given upload panel.

    Upload areas are defined by the :term:`application developer` or the
    :term:`server administrator`. As an end user you can just assign them
    to your :term:`upload types <upload type>`  in order to classify them.

  library volume

    A folder in the file system where *library uploads* are stored.


Memo commands
=============

The plugin defines the memo commands
:ref:`[file] <ug.memo.upload>` and
:ref:`[gallery] <ug.memo.gallery>`.
