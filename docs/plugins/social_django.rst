.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.social_django:

==============================================
The ``social_django`` plugin
==============================================

.. currentmodule:: lino_xl.lib.social_django

Third-party authentication.

See :mod:`lino_xl.lib.social_django`.


.. contents::
   :depth: 1
   :local:
