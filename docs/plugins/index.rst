.. _ug.plugins:

==============================
Plugin reference for end users
==============================

This is the reference documentation of Lino's plugin collection.
These pages are meant to make sense to :term:`end users <end user>`.
This is not a tutorial.
The features described here are used in many Lino applications.
Your particular :term:`Lino application` probably
doesn't use all of the plugins documented here, and it may (partly) override the
standard functionalities described here.
See :ref:`lf.plugins` for an introduction.


.. toctree::
   :maxdepth: 1

   about
   accounting
   addresses
   agenda
   albums
   avanti
   beid
   bevat
   bevats
   blogs
   boards
   changes
   checkdata
   comments
   contacts
   cal
   coachings
   countries
   courses
   cv
   dashboard
   dupable
   eevat
   excerpts
   finan
   gfks
   google
   groups
   help
   households
   humanlinks
   invoicing
   languages
   linod
   lists
   memo
   nicknames
   notes
   notify
   orders
   outbox
   periods
   phones
   publisher
   polls
   printing
   products
   reception
   rooms
   search
   sepa
   sheets
   shopping
   social_django
   sources
   storage
   subscriptions
   system
   teams
   tickets
   topics
   trading
   trends
   uploads
   users
   userstats
   vat
   wages
   working
   xl


.. _ug.plugins.github:
.. _ug.plugins.tinymce:
.. _ug.plugins.extensible:
.. _ug.plugins.properties:
.. _ug.plugins.vatless:

Deprecated plugins
==================
