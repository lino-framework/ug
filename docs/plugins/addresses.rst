.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.addresses:

.. module:: lino_xl.lib.addresses

==============================================
The ``addresses`` plugin
==============================================

.. currentmodule:: lino_xl.lib.addresses

The :mod:`lino_xl.lib.addresses` plugin adds the possibility of having more than
one postal address per :term:`partner`.

When this plugin is installed, your application gets a "Manage addresses" button
in the :attr:`overview` field of a partner.

See also :ref:`team.mt.addresses`.


.. contents::
   :depth: 1
   :local:


Concepts
========

.. glossary::

  address fields

    The database fields that make up a postal address. Here they are:

    >>> sorted(addresses.Address.ADDRESS_FIELDS)
    ['addr1', 'addr2', 'city', 'country', 'region', 'street', 'street_box', 'street_no', 'street_prefix', 'zip_code']

    Each partner object has these address fields, and each :term:`address
    record` has them.  The address fields of a partner always contain a copy of
    the partner's :term:`primary address`.

  address record

    One of possibly many addresses of a given partner.
    Stored using the :class:`Address` model.
    Implements :class:`lino.utils.addressable.Addressable`.

    Inherits fields from :class:`lino_xl.lib.countries.CountryRegionCity`
    (country, region, city. zip_code) and
    :class:`lino_xl.lib.contacts.AddresssLocation` (street, street_no, ...).

  primary address

    The address that is used as the primary address of a given partner.





Technical documentation
=======================

See :ref:`dg.plugins.addresses`.
