.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.topics:

=========================================================
The ``topics`` plugin
=========================================================

.. currentmodule:: lino_xl.lib.topics

Managing topics and those who are interested.

See :mod:`lino_xl.lib.topics`.


.. contents::
   :depth: 1
   :local:
