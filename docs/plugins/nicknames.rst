.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.nicknames:

========================================================
The ``nicknames`` plugin
========================================================

.. currentmodule:: lino_xl.lib.nicknames

The ``nicknames`` plugin adds a virtual field :attr:`my_nickname` to a model of
your application, letting users give their individual nickname to individual
rows of that model. On their dashboard they will then get a :term:`welcome
message` that lists all nicknamed database rows.

Giving a nickname is a pragmatic way for quickly creating a shortcut to an
often-visited database row.


.. contents::
   :depth: 1
   :local:


Glossary
========

.. glossary::

  nickname

    A name used by a given user to refer to a given database rows.

  nicknaming

    A :term:`database row` expressing the fact that a given :term:`site user`
    has given a nickname to a given thing.
