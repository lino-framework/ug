.. _ug.plugins.system:


===================================
The ``system`` plugin
===================================

This plugin defines some site-wide functionality like the :term:`site
parameters`, :term:`time zones <time zone>`, etc..

.. currentmodule:: lino.modlib.system

.. contents::
   :depth: 1
   :local:



.. glossary::

  site parameters

    A :term:`data window` where you can modify certain site-wide settings via
    the web interface.

    A :term:`site manager` can open this window via
    :menuselection:`Configuration --> System --> Site parameters`.

  time zone

    Lino stores timestamps internally in the UTC time zone, but when
    displaying them converts them into the user's time zone.

    Every user can select their preferred time zone in their :term:`user
    settings`.

    See also https://en.wikipedia.org/wiki/Time_zone

    The time zones available on a given :term:`Lino site` can be configured in
    :class:`TimeZones`.

    As a :term:`hosting provider` you can customize this :term:`choicelist` for
    a :term:`Lino site` in the :attr:`lino.core.site.Site.user_types_module`.

  date format

    A codified text like "dd.mm.yyyy", which instructs Lino how dates are to
    be represented.

    See https://en.wikipedia.org/wiki/Date_format_by_country

    Every :term:`Lino site` has its list of :class:`DateFormats`.
