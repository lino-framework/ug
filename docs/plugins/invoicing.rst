.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.invoicing:

================================
The ``invoicing`` plugin
================================

.. currentmodule:: lino_xl.lib.invoicing

The ``invoicing`` plugin adds functionality for automatically generating
invoices and other business documents.

.. contents::
   :depth: 1
   :local:

Overview
========

In applications with this plugin you usually have a command
:menuselection:`Sales --> My invoicing plan` in your main menu.

You also have a button |basket| in the toolbar of a :term:`partner`.

Both commands open your :term:`invoicing plan`, i.e. your plan to let Lino
generate a series of invoices. See `Your invoicing plan`_ below.

Before users can generate invoices, the  :term:`site manager` you must
configure a list of :term:`invoicing tasks <invoicing task>` available on this
site. This is described in `How to configure invoicing tasks`_.


Concepts
========

.. glossary::

  invoiceable

    A database object that can cause Lino to generate an :term:`invoice` or any
    other type of trading document.

    For example a :term:`working session` in :ref:`noi`  or an :term:`activity
    enrolment` in :ref:`voga` are :term:`invoiceable`.

  invoicing order

    A database object that can act as a starting point for running an
    :term:`invoicing plan`.


  invoicing suggestion

    One item of an :term:`invoicing plan`, which represents a single invoice
    that Lino suggests to generate.

    See :class:`Item` for a description of the individual fields.

  invoicing task

    A :term:`database row` that describes a group of business activities for
    which end users can start an :term:`invoicing plan`.

    Each invoicing task is also a :term:`background task` that can be scheduled
    to run automatically.

  invoicing event

    A database object that is part of a series and potentially causes a new
    contingent should be invoiced.

    can yield a series of :term:`invoiceables
    <invoiceable>` for a given date.

    invoicing events can be instances of any model,
    but the generator's :meth:`get_invoiceable_event_formatter`
    must understand them.

  event pass

    A product that, when sold, gives its owner the right to use (participate in)
    a given number of :term:`invoicing events <invoicing event>`.

  event pass type

    The type of an :term:`event pass`. It specifies the number of events, and
    other information like when to write a next invoice.

  invoice recipient

    The partner who gets the invoice.

  invoiceable partner

    The partner who asks for the service or good and who will consume or own it.

    This is usually synonym of "client" or "customer".

Your invoicing plan
===================

There is at most one :term:`invoicing plan` per user in the database.

.. glossary::

  invoicing plan

    A temporary :term:`database row` that represents the plan of a given user to
    let Lino generate a series of invoices (or other types of :term:`trading
    vouchers <trading voucher>`).

The :term:`invoicing plan` has a |lightning| button in the toolbar. Click on
this to fill your plan. Lino then gives a series of :term:`suggestions
<invoicing suggestion>`, one line for every invoice that Lino suggests to
generate.

Click on the |money| button of a suggestion to actually generate that invoice.

Or click on the |money| button in the toolbar to generate all suggested
invoices.

A mandatory field of an :term:`invoicing plan` is the :term:`invoicing task` to
use. The list of available invoicing tasks must have been configured by your
:term:`site manager`.

See :class:`Plan` for a description of the other fields.

Every :term:`invoicing plan` has a list of :term:`invoicing suggestions
<invoicing suggestion>`. To **update** the plan means to let Lino fill that
list. To **execute** the plan means to generate an invoice for each
suggestion.

How to configure invoicing tasks
================================

You must be :term:`site manager`.

Open the menu command :menuselection:`Configure --> Sales --> Invoicing tasks`.


Invoice recipients
==================

In applications with this plugin you can open the :term:`detail window` of any
partner to specify an :term:`invoice recipient`.

For example 10 children from an orphanage attend to different activities. The
orphanage pays for it.   The price of each activity depends on the age of the
child. We want to send the orphanage one invoice (not 10 invoices) every month.
On that invoice they want to see the costs separated per child. Every child is
an :term:`invoiceable partner`, the orphanage is :term:`invoice recipient`.
