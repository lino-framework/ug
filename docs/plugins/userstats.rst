.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.userstats:

==============================================
The ``userstats`` plugin
==============================================

.. currentmodule:: lino_xl.lib.userstats

User-related statistics.
See :mod:`lino_xl.lib.userstats`.


.. contents::
   :depth: 1
   :local:
