.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.reception:

=============================================================
The ``reception`` plugin
=============================================================

.. currentmodule:: lino_xl.lib.reception

The ``reception`` plugin is used to receive calendar guests using a waiting
queue.

See :mod:`lino_xl.lib.reception`.


.. contents::
   :depth: 1
   :local:
