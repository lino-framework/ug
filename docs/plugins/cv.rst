.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.cv:

==============================================
The ``cv`` plugin
==============================================

Manage career data about people and produce CVs for them.

.. currentmodule:: lino_xl.lib.cv

See :mod:`lino_xl.lib.cv`.


.. contents::
   :depth: 1
   :local:
