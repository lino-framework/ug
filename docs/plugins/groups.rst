.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.groups:

==========================
The ``groups`` plugin
==========================

The :mod:`lino_xl.lib.groups` plugin adds the notions of :term:`user groups
<user group>` and :term:`user memberships <user membership>`.


.. contents::
  :local:

Overview
========



.. glossary::

  user group

    A named list of :term:`user memberships <user membership>`.

  user membership

    The fact that a given :term:`site user` is member of a given :term:`user
    group`.


As a :term:`site manager` you can open :menuselection:`Configure --> System -->
User groups` to manage all user groups on this site.

As a :term:`site user` you can join or quit a group via the `Memberships` panel
in your :term:`user settings`.
