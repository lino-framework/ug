.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.tickets:

===============================================
The ``tickets`` plugin
===============================================

.. contents::
   :depth: 1
   :local:

Overview
========

.. glossary::

  ticket

    An issue, question, problem, help request or idea formulated by a human who
    asks our team to work on it. The smallest unit for organizing our teamwork.

The **author** of a ticket is the user who created it.

The **team** of a ticket is the :term:`user group` considered collectively
responsible for this ticket.

A ticket usually gets **assigned to** a :term:`site users <site user>`, a team
member considered **personally responsible** for working on it and who will see
this ticket as part of their `Tickets to do`_ list. Any :term:`site user` with
the :class:`Triager` role can reassign a ticket to another team member.

A ticket can optionally have an **end user**, which is a pointer to the
external :term:`person` who actually introduced this request by phone, email or
live via the author.

When the :mod:`invoicing <lino_xl.lib.invoicing>` plugin is installed,  a ticket
can optionally point to an :term:`invoicing order`, which will potentially cause
Lino to make a service report or invoice based on the work done for this ticket.


Tickets to do
=============

A list of the tickets you are assigned to.


See also
========

Developer documentation :ref:`dg.plugins.tickets`.
