.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.excerpts:

============================================
The ``excerpts`` plugin
============================================

The :mod:`lino_xl.lib.excerpts` plugin adds the notions of :term:`database
excerpt`.



.. contents::
   :depth: 1
   :local:

What is a database excerpt?
===========================

As an :term:`end user` you can see a history of the documents you asked Lino to
print by selecting :menuselection:`Office --> My excerpts`.

As a :term:`site manager` you can see  all the documents that any user
asked Lino to print by selecting :menuselection:`Explorer --> All excerpts`.

These "documents somebody asked Lino to print" have a name. We call them
:term:`database excerpts <database excerpt>`.

.. glossary::

  database excerpt

    A database object that represents the fact that a given user has requested a
    given printable document at a given moment.

End users can see a history of their printouts in :menuselection:`Office --> My
excerpts`.

When the end user clicks on a print button, Lino actually creates a database
excerpt, then builds the printable file and finally instructs the user's browser
to show this file.


Excerpt types
=============

Lino also has a table of **excerpt types** where the system administrator can
configure which types of database excerpts are available on a site. You can see
this list via :menuselection:`Configuration --> Excerpt types`.

.. glossary::

  excerpt type

    A configuration database object that specifies that a print button should
    appear for a given model.


The detailed structure of this table is documented on the :class:`ExcerptType`
model.

When a Lino process starts up, it automatically reads this table and
installs a "Print" action on every model of a site for which there
is an *excerpt type*.


Technical documentaton
======================

See :ref:`dg.plugins.excerpts`, :ref:`dg.topics.printing`
