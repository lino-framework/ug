.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.humanlinks:

===============================================
The ``humanlinks`` plugin
===============================================

Relation links between persons

.. currentmodule:: lino_xl.lib.humanlinks

See :mod:`lino_xl.lib.humanlinks`.


.. contents::
   :depth: 1
   :local:
