.. _ug.plugins.search:


======================================
the ``search`` plugin
======================================

.. currentmodule:: lino.modlib.search

Defines the :term:`site-wide search window`.

.. contents::
   :depth: 1
   :local:


.. glossary::

  site-wide search window

    Search for a text anywhere in the database, i.e. without even selecting a
    particular a :term:`data table`.
