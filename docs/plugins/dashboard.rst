.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.dashboard:

==============================================
The ``dashboard`` plugin
==============================================

This plugin lets site users configure their :term:`dashboard` from their
:term:`user settings`.

.. currentmodule:: lino_xl.lib.dashboard

See :mod:`lino_xl.lib.dashboard`.


.. contents::
   :depth: 1
   :local:
