.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.products:

=======================
The ``products`` plugin
=======================

.. currentmodule:: lino_xl.lib.products

The ``products`` plugin adds functionality for managing "products", iee.  things
that can be "traded" (e.g. purchased or sold).


.. contents::
   :depth: 1
   :local:

Overview
========

Immaterial products are usually called services.

This plugin is usually combined with the :mod:`lino_xl.lib.trading`
and/or the :mod:`lino_xl.lib.orders` plugin.

A :term:`trading voucher` is a business document that links a partner to N
products. Each item of a :term:`trading voucher` can point to maximally one
product at a time.
