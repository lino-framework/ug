.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.polls:

==============================================
the ``polls`` plugin
==============================================

.. currentmodule:: lino_xl.lib.polls

Managing questionnaires.

See :mod:`lino_xl.lib.polls`.


.. contents::
   :depth: 1
   :local:
