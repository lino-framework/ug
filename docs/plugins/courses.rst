.. _ug.plugins.courses:

===================================
The ``courses`` plugin
===================================

This plugin adds functionality for managing "activities". In :ref:`voga` they
are called "activities", in :ref:`avanti` they are called "courses", in
:ref:`tera` they are called "dossiers", in :ref:`welfare` they are called
"workshops", in :ref:`presto` they are called "deployments".  But they all have
certain things in common.

.. glossary::

  activity

    A :term:`database object` that represents the fact that a given
    :term:`activity leader` meets more or less regularly with a given group of
    :term:`participants <activity participant>`.

  activity meeting

    A :term:`calendar entry` that happens as part of an :term:`activity`.

An :term:`activity` can automatically generate :term:`calendar entries <calendar
entry>` (called "meetings", "lessons", "deployments", whatever) according to
:term:`recurrency rules <recurrency rule>`.  Lino helps with managing these
meetings: schedule exceptions and manual date changes.  It can fill the guests
or participants of the meetings, and the teacher can register their presence.
Activities can be grouped into :term:`activity lines <activity line>` (series),
series into *topics*.


.. glossary::


  activity enrolment

    The fact that a person has declared to participate in an activity.

  activity participant

    A person who is enrolled in an activity and usually is present at every meeting.

  activity leader

    The person who is usually present as leader of each meeting.

The activities of a site differ by their :term:`activity line` and their
:term:`activity layout`.

.. glossary::

  activity line

    A line --or series-- of activities.

    Used to group activities into a configurable list of categories.

    We chose the word "line" instead of "series" because it has a plural form.

  activity layout

    A hard-coded way to differentiate and giving names to the major types of
    activities of an application.


Activities are :term:`event generators <event generator>`.
See also :doc:`/topics/cal_auto`.



Pupils and teachers
===================

While the standard :mod:`lino_xl.lib.courses` plugin allows any :term:`person`
to act as :term:`activity leader` :term:`activity participant`, Lino Voga (for
example) introduces specific database models for teachers and pupils.

.. glossary::

  teacher

    A :term:`person` who can act as an :term:`activity leader`.

    Teachers are stored in the database using the :class:`Teacher` model.

  pupil

    A :term:`person` who can act as an :term:`activity participant`.

    Pupils are stored in the database using the :class:`Pupil` model.

  teacher type

    The type of a :term:`teacher`.

  pupil type

    The type of a :term:`pupil`.
