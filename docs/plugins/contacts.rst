.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.contacts:

================================
The ``contacts`` plugin
================================

.. currentmodule:: lino_xl.lib.contacts

The ``contacts`` plugin adds functionality for managing your contacts. It
defines concepts like :term:`business partner`, :term:`person`
:term:`organization` and :term:`contact person`.

.. contents::
   :depth: 1
   :local:



Business partners
=================

.. glossary::

  business partner

    A legal or natural person for which you want to keep
    contact data like postal address, phone number, etc.

    A :term:`business partner` can act as the recipient of a :term:`sales
    invoice`, as the sender of a :term:`purchase invoice`, ... A partner has at
    least a name and usually also an address.

    Stored using the :class:`Partner` database model.
    You can see the partners in your database via
    :menuselection:`Explorer --> Contacts --> Partners`.

    ..
      >>> show_menu_path('contacts.Partners')
      Contacts --> Persons

  partner

    Short for :term:`business partner`.

A :term:`business partner` is always either a (natural)
:term:`person` or an :term:`organization`.

.. glossary::

  person

    A natural human person with a gender, first and last name.

    Stored using the :class:`Person` database model.
    You can see the persons in your database via :menuselection:`Contacts -->
    Persons`.

  organization

    A corporation, company, organization, family or any other potential
    :term:`business partner` that is *not* a :term:`person`.

    Stored using the :class:`Company` database model.
    You can see the organizations in your database via
    :menuselection:`Contacts --> Organizations`.

Note how persons and organizations have similarities and differences.  For
example they all have an `address` and a `phone number` field.   But persons
have a e.g. `Last name`, `First name` and `Sex` fields while organizations have
an `Organization type` field.
See also :ref:`lino.tutorial.human`.

The **Partners** table is what *persons* and *organizations* have in common. As
you can see, this table contains both your persons *and* your organizations
**together** in a same list.  It is the `union
<https://en.wikipedia.org/wiki/Union_%28set_theory%29>`_ of both tables.

Why do we need such a union table of business partners? For example an invoice (one of
the important documents used in accounting) must have a *recipient*, and that
recipient can be a private person for some invoices and an organization for some
other invoices. And (last but not least) in many accounting situations you are
not interested whether it's a person or an organizations, it is just some
business partner who owes you money.

Lino applications can add additional types of business partners: households,
employees, patients, beneficiaries, teachers, shareholders, ...


Linking persons and organizations
==================================

A person can have a given *role* in a given organization and thus becomes a
*contact person*.

A :term:`contact person` is when a given *person* exercises a given *function*
in a given *organization*. A :term:`contact function` is what a given
:term:`person` can exercise in a given :term:`organization`.

.. glossary::

    contact person

      The fact that a given :term:`person` exercises a given function
      within a given :term:`organization`.

      The :guilabel:`Contact persons` panel of an organization's :term:`detail
      window` shows the contact persons of this organization. The :guilabel:`Is
      contact for` panel of a person's :term:`detail window` shows the
      organizations where this person exercises a function.

      Contact person entries are stored using the :class:`Role` database model.

    contact function

      A function that a person can exercise in an organization.
      Represented by :class:`RoleType`.

    signer function

      A :term:`contact function` that has :attr:`can_sign <RoleType.can_sign>`
      set to True.

      A contact person exercising a signer function is allowed to sign business
      documents. See :meth:`Partner.get_signers`.



Quickly finding a partner
===============================================

In a partners table you can quick-search for the primary phone number.

A special type of quick search is when the search string starts with
"#".  In that case you get the partner with that primary key.

This behaviour is the same for all subclasses of Partner, e.g. for
persons and for organizations.



Setting the site operator
=========================

Check whether you want Lino to know who is the :term:`site operator`. For
example when you issue a sales invoice, Lino will use this to print your address
and contact data on the invoice. See :ref:`ug.site_company`.
