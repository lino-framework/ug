.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.periods:

========================================================
The ``periods`` plugin
========================================================

.. currentmodule:: lino_xl.lib.periods

The :mod:`lino.modlib.periods` plugin defines the database models for storing
:term:`fiscal years <fiscal year>`  and :term:`accounting periods <accounting
period>` in the database.  The designations themselves ("fiscal year" and
"accounting period") can be customized e.g. into "academic years", "semesters",
"trimesters", or "quarters".

.. contents::
   :depth: 1
   :local:


Glossary
========

.. glossary::

  accounting period

    The smallest time slice to be observed in accounting reports. It usually
    corresponds to one *month*, but you can choose to report per quarter, per
    trimester or per semester.

  fiscal year

    A sequence of consecutive :term:`accounting periods <accounting period>`
    covered by an annual business activity report.

    A fiscal year often corresponds to the calendar year, but there can be
    situations where a company uses a shorter or a longer fiscal year.

    This usually corresponds to a calendar year. But not always. Exceptions may
    be companies having a shifted annual report or transitional periods.

    When the annual report has been published (declared to the national tax
    office), the fiscal year should be closed to prevent accidental changes in
    the ledger of that year.

See also
========

The :ref:`Developer Guide <dg.plugins.periods>` gives examples of exceptional
configurations like shifted years or periodicities other than monthly.
