.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.linod:

============================================
The ``linod`` plugin
============================================

.. currentmodule:: lino.modlib.linod

The :mod:`lino.modlib.linod` plugin adds a framework for defining and handling
:term:`background tasks <background task>`. Background tasks can be used for
example for sending out notification emails or running maintenance tasks such as
:manage:`checkdata` or :manage:`checksummaries`.


.. contents::
   :depth: 1
   :local:

Menu commands
==============

This plugin adds the following menu commands:

- :menuselection:`Configure --> System --> System tasks` is where a :term:`site
  manager` can monitor and configure :term:`system tasks <system task>`.

- :menuselection:`Explorer --> System --> Background procedures` shows the
  available :term:`background procedures <background procedure>`.


Glossary
========

.. glossary::

  background task

    A :term:`database row` where the :term:`end user` specifies when to run a
    given :term:`background procedure` on this site.

  background procedure

    A concrete job defined by a plugin for being run in background.

  system task

    A plain :term:`background task` without any specialization.

    For example :term:`invoicing tasks <invoicing task>` are specialized
    background tasks that exist only when the :doc:`invoicing <invoicing>`
    plugin is installed.

  background task runner

    The service program that runs background tasks when they are scheduled to
    run.
