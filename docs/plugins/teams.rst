.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.teams:

==========================
The ``teams`` plugin
==========================

The :mod:`lino_xl.lib.teams` plugin

.. contents::
  :local:
