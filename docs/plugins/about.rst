.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.about:

===============================================
The ``about`` plugin
===============================================

.. currentmodule:: lino_xl.lib.about

This plugin shows information about this :term:`Lino site`.
See :mod:`lino.modlib.about`.


.. contents::
   :depth: 1
   :local:
