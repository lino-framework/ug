.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.albums:

==================================
The ``albums`` plugin
==================================

.. currentmodule:: lino_xl.lib.albums

The ``albums`` plugin adds functionality for managing your photos.

.. contents::
   :depth: 1
   :local:
