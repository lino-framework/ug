.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.google:

====================================
The ``google`` plugin
====================================

Synchronize contacts and calendar data between a :term:`Lino site` and Google
for the accounts of site users who gave their permission.

See also the :ref:`developer documentation <dg.plugins.google>`.


.. contents::
  :local:
