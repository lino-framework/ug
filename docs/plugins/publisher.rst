.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.publisher:

========================================
The ``publisher`` plugin
========================================

The :mod:`lino.modlib.publisher` plugin adds a another way of rendering database
content for public websites. It also adds the notions of "content nodes", which
are the basis for web content pages and printable documents.


.. contents::
   :depth: 1
   :local:

Glossary
========

.. glossary::

  content node

    An arbitrary text with an optional title and optional a list of children,
    i.e. sub content nodes.


Technical documentation
=======================

See :ref:`dg.plugins.publisher`.
