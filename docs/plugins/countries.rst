.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.countries:

===============================================
The ``countries`` plugin
===============================================

.. currentmodule:: lino_xl.lib.countries

See :mod:`lino_xl.lib.countries`.


.. contents::
   :depth: 1
   :local:
