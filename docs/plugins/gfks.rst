.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.gfks:

==============================================
The ``gfks`` plugin
==============================================

Generic foreign keys

.. currentmodule:: lino_xl.lib.gfks

See :mod:`lino_xl.lib.gfks`.


.. contents::
   :depth: 1
   :local:
