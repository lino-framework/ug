.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.agenda:

==================================
The ``agenda`` plugin
==================================

.. currentmodule:: lino_xl.lib.agenda

The ``agenda`` plugin adds functionality for managing agendas.

.. contents::
   :depth: 1
   :local:

.. glossary::

    agenda item

      One of the things to discuss during this meeting.

      The items of a meeting are numbered sequentially to form an ordered list.
