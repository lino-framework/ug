.. _ug.plugins.finan:

=============================
The ``finan`` plugin
=============================

.. currentmodule:: lino_xl.lib.finan

The :mod:`lino_xl.lib.finan` plugin adds notions like :term:`financial voucher`
and :term:`booking suggestion`.

Table of contents:

.. contents::
   :depth: 1
   :local:

Vocabulary
==========

There are three kinds of financial vouchers:

.. glossary::

  bank statement

    A :term:`ledger voucher` received from your bank and which reports the
    transactions that occurred on a given bank account during a given period.

    See `Bank statements`_.

  payment order

    A voucher you send to your bank asking them to execute a series of payments
    (outgoing transactions) to third-party partners from a given bank account.

    See `Payment orders`_.

  journal entry

    A voucher where you declare that you move money around internally, for your
    own accounting.

    French: "operations diverse".

    See `Journal entries`_.

  financial voucher

    General term for the voucher types :term:`bank statement`, :term:`payment
    order` and :term:`journal entry`.  They have certain things in common, but
    use different database models because certain things differ.

  expected movement

    A :term:`ledger movement` that did not yet happen but is expected to happen.
    For example the payment of an invoice.

  booking suggestion

    A suggestion to add an :term:`expected movement` to a :term:`financial voucher`.
    See `Booking suggestions`_.

Payment orders
==============

A payment order clears the invoices it asks to pay.  Which means for example
that a supplier might tell you that some invoice isn't yet paid, although your
MovementsByPartner says that it is paid. The explanation for this difference is
simply that the payment order hasn't yet been executed by your or their bank.

Lino books the **sum of a payment order** into a single counter-movement that
will *debit* your bank's partner account (specified in the :attr:`partner
<Journal.partner>` of the journal).  Your bank becomes a creditor (you owe them
the sum of the payments) and you expect this amount to be cleared by an expense
in a :term:`bank statement` which confirms that the bank executed your payment
order.

Bank statements
===============

Cash journals
=============

Cash journals are technically the same as bank statements.


Journal entries
===============


Booking suggestions
===================

In a financial voucher you often book transactions that are actually expected.
For example, when you have booked an invoice, then Lino knows that each invoice
will eventually lead to a payment.
