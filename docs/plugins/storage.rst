.. _ug.plugins.storage:

================================
The ``storage`` plugin
================================

The ``storage`` plugin is about storing the things you own in different places
and about moving them around. You need it --obviously-- for  :term:`warehouse`
management applications, but also --less obviously-- for managing :term:`time
credit` at a :term:`service provider` or :term:`multiple entry passes <multiple
entry pass>` at a theater or swimming pool.

.. contents::
  :local:

Glossary
========

.. glossary::

  multiple entry pass

    A ticket by which a customer purchases a certain number of entries, usually
    at a reduced price per entry.

  time credit

    A number of working hours purchased by a customer who pays for service in
    advance.

  warehouse

    A building where raw materials or manufactured goods may be stored
    prior to their distribution for sale.

Provisions
==========

.. glossary::

  provision

    A given quantity of a given product (or service) for which a given partner
    has initiated a transaction.

  provision state

    The state of a :term:`provision` within its life cycle.

Typical examples of :term:`provision states <provision state>` are "in stock",
"ordered by customer", "ordered from provider", "rented out", "damaged", "under
repair" or "purchased".

For example, when customer Albert orders 50 light bulbs, Lino might create a
:term:`provision` "partner: Albert; product: light bulb; quantity: 50; state:
ordered". When Albert got his light bulbs, the storage state changes to
"delivered" and when the invoice has been sent the state changes to "sold".

For some provision states the partner is irrelevant. For example when we see that
3 books are "ordered by customer", we need to know the customer who ordered
them, but when we have 30 books "in stock", then these books aren't assigned to
a given partner.

In :ref:`noi1r <dg.projects.noi1r>` we use only one :term:`provision state`
named "purchased", and two :term:`transfer rules <transfer rule>`::

  $ go noi1r; pm show storage.TransferRules
  ===== ======================= =========== ===========
   No.   Journal                 From        To
  ----- ----------------------- ----------- -----------
   1     Sales invoices (SLS)                Purchased
   2     Service reports (SRV)   Purchased
  ===== ======================= =========== ===========

- A service report (SRV) *debits* "purchased"
- A sales invoice (SLS) *credits* "purchased"

The customers in noi1r buy "Regular" services in advance, and each customer has
a provision of credit hours.

::

  $ go noi1r; pm show storage.Provisions
  ==== ===================== ========= ================= ===========
   ID   Partner               Product   Provision state   qty
  ---- --------------------- --------- ----------------- -----------
   1    Rumma & Ko OÜ         Regular   Purchased         10:00
   2    Bäckerei Ausdemwald   Regular   Purchased         20:00
   3    Bäckerei Mießen       Regular   Purchased         19:28
   4    Bäckerei Schmitz      Regular   Purchased         11:12
   5    Garage Mergelsberg    Regular   Purchased         10:00
  ==== ===================== ========= ================= ===========


.. glossary::

  transfer rule

    A configuration :term:`database row` that specifies when a given
    :term:`ledger voucher` "transfers" a product from one :term:`provision` to
    another.

And there would be only one product with storage management enabled, and
that product would be named "Time credit". Maybe there will be multiple
products in order to differentiate between "temporary" (befristet) and
"timeless" (unbefristet) time credit.

Fillers
=======

A *service level agreement* will somehow specify a "required provision", which
means that the customer must always have a minimum provision of time credit. For
example they buy 10 hours, which are being used up with each working session,
and when the time credit gets used up they want to buy new credit. This is done
using **fillers**. For each subscription that has such a "required provision",
Lino can check whether the partner still has enough provision. If not, it would
generate an invoice to buy new time credit.

::

  $ go noi1r; pm show storage.Fillers
  ==== ===================== ================= ================ =============== ============
   ID   Partner               Provision state   Wanted product   Minimum asset   Fill asset
  ---- --------------------- ----------------- ---------------- --------------- ------------
   1    Rumma & Ko OÜ         Purchased         Regular          2:00            10:00
   2    Bäckerei Ausdemwald   Purchased         Regular          2:00            20:00
   3    Bäckerei Mießen       Purchased         Regular          2:00            50:00
   4    Bäckerei Schmitz      Purchased         Regular          2:00            90:00
   5    Garage Mergelsberg    Purchased         Regular          2:00            10:00
  ==== ===================== ================= ================ =============== ============


To specify this, we could simply add three new fields:

- "product" : a pointer to the "Time credit" product
- a minimum quantity (e.g. 5 hours)
- an order quantity (e.g. 10 hours)

We have two basic approaches for generating invoices:

- "period-based invoicing" for subscriptions with a monthly or yearly fee.
  For example an :term:`activity enrolment` with
  a start_date and an end_date, and a :term:`subscription periodicity` gives
  `renew_unit`, `renew_every` and `renew_before`.

- "occurence-based invoicing" :
  Lino needs a `number_of_events`, a `min_asset` and `max_asset`.
  This is used e.g. in Voga when a customer buys a
  "multi-pass card" for 10 sessions of an activity.


Fillers and provisions are about *quantity*, not about *price*. When a provision
reaches below the minimum asset quantity in the filler, the customer will order
new products, and if the product's price has changed in the mean time, the newly
bought product's prices will be different.

Q: How to handle quantity-related prices?  For example the unit price of an hour
would be 90 € while 5 hours would cost only 400 € and 10 hours only 750 €.

A: This is not related to the storage plugin and should be addressed by "price
rules" or "complex products".

See also
========

Developer documentation: :ref:`dg.plugins.storage`
