.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.printing:

=========================================
The ``printing`` plugin
=========================================

The :mod:`lino.modlib.printing` plugin  contains general printing functions. It
can add a print button to any database object. Which objects are printable and
how the result is to look like, depends of course on the application and the
site configuration.

.. contents::
  :local:
