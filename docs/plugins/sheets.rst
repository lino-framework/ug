.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.sheets:

==============================================
The ``sheets`` plugin
==============================================

.. currentmodule:: lino_xl.lib.sheets

The ``sheets`` plugin is used to produce annual financial reports.

See :mod:`lino_xl.lib.sheets`.


.. contents::
   :depth: 1
   :local:
