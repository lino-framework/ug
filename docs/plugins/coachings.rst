.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.coachings:

==============================================
The ``coachings`` plugin
==============================================

A coaching is when a given :term:`site user` (e.g. a social worker or a
therapist or a teacher) is responsible for a given :term:`person`.

.. currentmodule:: lino_xl.lib.coachings

See :mod:`lino_xl.lib.coachings`.


.. contents::
   :depth: 1
   :local:
