.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.memo:

==============================================
The ``memo`` plugin
==============================================

.. currentmodule:: lino_xl.lib.memo

This plugin adds the functionality for rendering :term:`memo commands <memo
command>`. It defines a simple markup language.

.. See :mod:`lino_xl.lib.memo`.


.. contents::
   :depth: 1
   :local:
