.. doctest docs/plugins/subscriptions.rst
.. _ug.plugins.subscriptions:

========================================================
The ``subscriptions`` plugin
========================================================

.. currentmodule:: lino_xl.lib.subscriptions

The :mod:`subscriptions <lino_xl.lib.subscriptions>` plugin adds functionality
for managing agreements that cause periodic invoices to be generated. For
example a subscription to a newspaper, a service level agreement, a yearly
membership, ...

.. contents::
  :local:


Glossary
=========

.. glossary::

  subscription

    An agreement that causes periodic invoices to be generated.

  subscription periodicity

    The frequency of renewal invoices. For example "monthly", "quarterly" or
    "yearly".

  subscription period

    A date range within a given :term:`subscription` for which an invoice is to
    be (or has been) generated.


Subscription periods
====================

Once you have created a :term:`subscription`, Lino will automatically generate a
number of :term:`subscription periods <subscription period>` for this
subscription.


See also
========

Developer documentation: :ref:`dg.plugins.subscriptions`
