.. include:: /../docs/shared/include/defs.rst

.. _ug.plugins.users:

================================================
The ``users`` plugin
================================================

.. contents::
  :local:

Overview
============

The :mod:`lino.modlib.users` plugin is used in most Lino applications. It
defines the database table for storing :term:`user accounts <user account>`,
actions for :term:`signing in <sign in>` and :term:`out <sign out>`, editing
:term:`user settings`. It also manages the :term:`permissions <user
permissions>` given to each user.

User settings
================

You can open this window by clicking on the :term:`My settings` quick link or
selecting the menu item of same name in the :term:`user menu`.

User settings are stored in the database using
the :class:`lino.modlib.users.User` :term:`database model`.

.. For a technical description of the fields, see :class:`lino.modlib.users.User`.

User settings are the part of a :term:`user account` that can be edited by the
users themselves.

See also :ref:`ug.howto.password`.

Manage user accounts
======================

As a :term:`site manager` you can add new users and edit existing users via the
:menuselection:`Configure --> System --> Users` menu command.

Double-clicking on a row in this data table opens the `User settings`_ of
that user.

See also :ref:`ug.howto.users`.


The site manager
================

A :term:`site manager` is a privileged :term:`site user` who is responsible for
managing the list of :term:`user accounts <user account>` on a given :term:`Lino
site`.

A :term:`site manager` never needs to specify the current password when setting
a new password for any user account.

A :term:`site manager` can optionally specify a date when a user started or
stopped to be active.


User types and user roles
=========================

You can see the user types available on your :term:`Lino site` via
:menuselection:`Explorer --> System --> User Types`.  Here is a typical list of
user types:

======= =========== ===============
 value   name        text
------- ----------- ---------------
 000     anonymous   Anonymous
 100     user        User
 900     admin       Administrator
======= =========== ===============

Another menu command might be interesting: :menuselection:`Explorer --> System
--> User roles`.  This table shows the :term:`user roles <user role>` defined on
your site and for each :term:`user type` whether it has that role or not.  Lino
consults this table when deciding whether to give permission (or not) to see
certain menu commands.

======================== ===== ===== =====
 Name                     000   100   900
------------------------ ----- ----- -----
 comments.CommentsStaff               ☑
 comments.CommentsUser          ☑     ☑
 contacts.ContactsStaff               ☑
 contacts.ContactsUser          ☑     ☑
 excerpts.ExcerptsStaff               ☑
 excerpts.ExcerptsUser          ☑     ☑
 office.OfficeStaff                   ☑
 office.OfficeUser              ☑     ☑
 xl.SiteAdmin                         ☑
 xl.SiteUser                    ☑
======================== ===== ===== =====

These tables are maintained by the :term:`application developer`. You cannot
edit them. They are interesting for end users who want to understand what every
user type does. If you think that something is wrong with the :term:`user
permissions` on your site, then talk about user roles with your :term:`site
expert`.


Acting as another user
======================

There is a menu entry :menuselection:`Act as...` in the :term:`user menu`, which
you use in situations where you act in the name of another user.

That other user must have given you :term:`authority` to do so. Except if you
are a :term:`site manager`, because a :term:`site manager` can act as anybody
else without having an explicit authority.

Use cases:

- calendar entries for a social agent can be made by a reception clerk
- One user creating a comment while working as another user

TODO: write more explanations.


Online registration
===================

Your :term:`Lino site` may have :term:`online registration` enabled.

TODO: write more explanations.

.. _ug.plugins.users.third_party_authentication:

Third-party authentication
==========================

The :setting:`users.third_party_authentication` feature can be enabled by the
:term:`server administrator`. When this is enabled, you must also create
"applications" on some third-party auth provider (Google, Facebook).

TODO: write more explanations.



Authentication
==============

Authentication is the process that happens when an :term:`end user` :term:`signs
in <sign in>`.  It includes:

- Ask for user credentials (username and password)
- Look up the :term:`user account` from the database.
- Verify whether password is valid
- Optionally use other authentication methods
- Store the :class:`users.User` instance in the :class:`Session` instance.


Glossary
========

This documentation page introduces the following concepts.

.. glossary::

  user account

    A :term:`database row` with information about a given :term:`site user`.

  My settings

    A :term:`quick link` and an entry in the :term:`user menu` that opens a
    detail window where you can edit your :term:`user settings`.

  user type

    The type of a :term:`user account`, which defines the user's
    :term:`permissions <user permissions>`. See `User types and user roles`_.

  user role

    A role within the application that can be assigned to a given :term:`user
    type`. User roles are the atomic units for handling :term:`user permissions`
    in Lino. See `User types and user roles`_.

  user permissions

    The set of functionalities and data to which a given :term:`site user` has
    access.  This set is defined by the :term:`user type`.

  authority

    The fact that one user gives another user the right to act in their name.
    See `Acting as another user`_.

  online registration

    A feature of a :term:`Lino site` that allows new :term:`end users <end
    user>` to create a :term:`user account` without (or with little) interaction
    of the :term:`site manager`. See `Online registration`_.

  Sign in

    The action of telling a :term:`Lino site` who you are. See Authentication_.

  Sign out

    Tell Lino that you no longer want to be
    treated as an authenticated user and want to become anonymous again.

    This is an item of the :term:`user menu`.

  demo mode

    Operation mode of a :term:`Lino site` where  the welcome text for anonymous
    users says "This demo site has X users, they all have "1234" as password",
    followed by a list of available usernames. A visitor can sign in just by
    clicking on a user name.

    The demo mode should obviously be switched off on a :term:`production site`.

    The demo mode is activated by setting
    :attr:`lino.core.site.Site.is_demo_site` to `True` in your
    :xfile:`settings.py` file.

  verification code

    A code sent to a user via email for verification.
