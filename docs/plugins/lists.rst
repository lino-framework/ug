.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.lists:

===============================================
The ``lists`` plugin
===============================================

.. currentmodule:: lino_xl.lib.lists

See :mod:`lino_xl.lib.lists`.


.. contents::
   :depth: 1
   :local:
