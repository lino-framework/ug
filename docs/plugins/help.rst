.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.help:

=================================================
The ``help`` plugin
=================================================

.. currentmodule:: lino_xl.lib.help

The ``help`` plugin adds a :kbd:`?` button to many places and generates
:term:`local help pages`

.. contents::
   :depth: 1
   :local:

.. glossary::

  site contact

    A partner who has some role related to this :term:`Lino site`. For example
    the :term:`site operator`, the :term:`server administrator` and the `support
    provider` are usually well-defined legal persons.

The site contacts of your site are user in the :class:`lino.modlib.about.About`
dialog window and in the :term:`local help pages`.
