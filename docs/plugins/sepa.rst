.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.sepa:

==============================================
The ``sepa`` plugin
==============================================

.. currentmodule:: lino_xl.lib.sepa

The ``sepa`` plugin is used to communicate with banks using SEPA.

See :mod:`lino_xl.lib.sepa`.


.. contents::
   :depth: 1
   :local:
