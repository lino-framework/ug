.. include:: /../docs/shared/include/defs.rst
.. _ug.plugins.avanti:

==============================================
The ``avanti`` plugin
==============================================

The ``avanti`` plugin is the main plugin for :ref:`avanti`.

.. currentmodule:: lino_xl.lib.avanti

See :mod:`lino_xl.lib.avanti`.


.. contents::
   :depth: 1
   :local:
