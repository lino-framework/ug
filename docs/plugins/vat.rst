.. _ug.plugins.vat:

=================================
The ``vat`` plugin
=================================

.. currentmodule:: lino_xl.lib.vat

The :mod:`lino_xl.lib.vat` plugin adds notions for handling value-added tax
(VAT).

Table of contents:

.. contents::
   :depth: 1
   :local:

Overview
========

The VAT plugin defines the following concepts:

.. glossary::

  VAT number

    An identification number used by national tax offices to identify legal
    persons that are subjectible to VAT.

    See also `Wikipedia <https://en.wikipedia.org/wiki/VAT_identification_number>`__.

    See also :attr:`lino_xl.lib.contacts.Partner.vat_id`.

  VAT id

    Short for :term:`VAT identification number`.

  VAT identification number

    Another word for :term:`VAT number`.

  VAT rate

    The percentage to apply to a base amount in order to compute the amount of
    VAT.

  VAT regime

    Specifies how the VAT for this voucher is being handled, e.g. which VAT
    rates are available and whether and how and when VAT is to be declared and
    paid to the national :term:`tax office`.  See `VAT regimes`_.

  VAT class

    The nature of a trade object to be differentiated in the :term:`VAT
    declaration`.

    For example most countries differentiate between "goods" and "services".
    Other common VAT classes are "investments" or "vehicles".

    The list of available VAT classes is defined by your :term:`national VAT
    module`. For example in Estonia the VAT office wants to know, in one field
    of your declaration, how much money you spend for buying "vehicles" and in
    another field how much you spent for "real estate" objects, while in Belgium
    both vehicles and real estate objects are considered together as
    "investments".

  VAT rule

    A rule that defines which :term:`VAT rate` to apply and which account to use
    for a given combination of regime, class and trade type. The list of
    available VAT rules depends on which :term:`national VAT module` is
    installed.

  VAT area

    A group of countries having same :term:`VAT rules <VAT rule>` in the country
    of the :term:`site operator`.  See `VAT areas`_.

  national VAT module

    The module that implements a given national VAT configuration.

    Every :term:`Lino site` has at most one :term:`national VAT module`,
    which corresponds to the country where the :term:`site operator` is
    registered.

    Specified by :attr:`lino_xl.lib.vat.Plugin.declaration_plugin`.

  tax office

    The business partner (organization) to whom you must pay due VAT or who
    returns your deductible VAT.

    The tax office is for a :term:`VAT declaration` what the customer is for a
    sales invoice, or the provider for a purchases invoice.


VAT declarations
================

A :term:`VAT declaration` has a number of fields that contain different sums,
automatically computed by Lino when you register the voucher. These sums reflect
the operations in an **observed period** or range of periods. The voucher itself
is a ledger voucher that generates new movements in its own **period of
declaration**, which is different from the observed period range.


.. glossary::

  VAT declaration

    A document to be submitted every month or every quarter to your national
    :term:`tax office`.  It is a summary of the value of your sales and purchase
    transactions during a given period, showing the due VAT and the deductible
    VAT.  Details are implemented by the :term:`national VAT module`.

    See also :ref:`ug.howto.vat`.

  intra-community statement

    A document to be submitted together with your :term:`VAT declaration` when
    you had intra-community sales operations. It is a list of all non-domestic
    intra-community customers with their sum of sales operations.

    fr: "relevé intracommunautaire", "listing des clients intracommunautaires",
    de: "Liste der innergemeinschaftlichen Kunden"

  annual list of domestic customers

    A list with each domestic VAT subjectible partner with whom you had sales
    operations that exceed 250€

    fr: "liste annuelle des clients assujettis", de: "jährliche Liste der
    MwSt-pflichtigen Kunden"




VAT regimes
===========

A :term:`VAT regime` must be assigned to each voucher, and the default value to
be used in new vouchers can optionally be set for each partner.

The *VAT regime of a partner* is used as the default value for all vouchers
with this partner.  When you define a *default VAT regime* per partner, any new
voucher created for this partner will have this VAT regime.  Existing vouchers
are not changed when you change this field.

The :term:`VAT regime` has nothing to do with the :term:`trade type`. For
example, when a partner has the regime "Intra-community", this regime is used
for both sales and purchase invoices with this partner.  The difference between
sales and purchases is defined by the :term:`VAT rules <VAT rule>`, not by the regime.

.. _ug.plugins.vat.classes:

VAT classes
===========

A :term:`VAT class` is a direct or indirect property of a trade object (e.g. a
Product) that influences the VAT rate to be used.   Here is the list of VAT
classes used unless you have some local change.

======= ============= ===========================
 value   name          text
------- ------------- ---------------------------
 010     goods         Goods at normal VAT rate
 020     reduced       Goods at reduced VAT rate
 030     exempt        Goods exempt from VAT
 100     services      Services
 200     investments   Investments
 210     real_estate   Real estate
 220     vehicles      Vehicles
 300     vatless       Without VAT
======= ============= ===========================

The list of VAT classes does not contain the actual :term:`VAT rates <VAT rate>`
because this still varies by country, and within a same country it can vary,
e.g. depending on the date of the operation or other factors.

A :term:`VAT class` is assigned to each item of an invoice.  The VAT class can
influence the available VAT rates. You can sell or purchase a same product to
different partners using different VAT regimes.


VAT areas
=========

When your business is located, e.g. in Belgium and you buy goods or services
from France, Germany and the Netherlands, then these countries are called "intra
community". Certain :term:`VAT rules <VAT rule>` apply for all intra-community countries.
We don't want to repeat them for each country. That's why we have VAT areas.

======= =============== ===============
 value   name            text
------- --------------- ---------------
 10      national        National
 20      eu              EU
 30      international   International
======= =============== ===============

A :term:`VAT area` is a group of countries for which same :term:`VAT rules <VAT rule>`
apply in the country of the :term:`site operator`.

VAT areas are used to group countries into groups where similar VAT regimes
are available.

The :attr:`country <lino_xl.lib.contacts.Partner.country>` field of a
:term:`partner` influences which `VAT regimes`_ are available for this partner.


Returnable VAT
==============

.. glossary::

  returnable VAT

    A VAT amount in an invoice that is not to be paid to (or by) the
    partner but must be declared in the VAT declaration.

    Returnable VAT, unlike normal VAT, does not increase the total amount of the
    voucher but causes an additional movement into the account configured as
    "VAT returnable" (a :term:`common account`).
