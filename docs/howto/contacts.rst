.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.contacts:

========
Contacts
========

.. contents::
   :depth: 1
   :local:


Create a new customer and their contact data
============================================

- In the :term:`main menu`, select :menuselection:`Contacts --> Persons` to
  register a *natural person*, or :menuselection:`Contacts --> Organizations`
  for *legal persons* like companies, unions, agencies, schools, institutions,
  etc.

- Click the |insert| button in the :term:`toolbar` or hit the :kbd:`Insert` key.

- Fill out the fields of the :term:`insert window` and hit the
  :guilabel:`Create` button to submit the window.

Lino contact management does not differentiate between customers and providers.
We say that a :term:`partner` is either a :term:`person` or an
:term:`organization`. Any partner can become a "customer" when you record a
:term:`sales invoice` for them, or a "provider" when you register a
:term:`purchase invoice` for them.

Create a partner as a copy of an existing partner
=================================================

- Open some :term:`data window` and navigate to the existing person or
  organization you want to copy.

- Click the :guilabel:`⚇` button ("Duplicate the selected row").


Merge duplicate partners
========================

When you discover that you have a **duplicate** :term:`person` or
:term:`organization`, i.e. two database entries where actually there should be
only one, you can **merge** them as follows:

- Select one of the partners

- Click the |merge| button. Lino asks:

    | **Merge**
    | into...: ____
    | ☐ Also reassign volatile related objects
    | Reason: ____

- Select the other partner in the :guilabel:`into...` field.

- Optionally specify a reason (which will just be logged in in your site's
  :xfile:`lino.log` file). Leave the :guilabel:`Also reassign volatile related
  objects` option unchecked unless you have been told to check it.

- Click the OK button. Before actually merging the partners, Lino will summarize
  what is going to happen and ask your confirmation:

    | Are you sure you want to merge Mr Hans Altenberg into Mr Alfons Ausdemwald?
    | 1 Bank Statement items, 2 Movements, 1 Sales invoices, 2 Partner balances will get reassigned.
    | Mr Hans Altenberg will be deleted.

- Click OK again to execute the merge.  A merge usually cannot be undone.

Find duplicate partners
=======================
