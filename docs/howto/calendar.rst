.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.calendar:

====================
Use the calendar
====================

.. contents::
   :depth: 1
   :local:


Create a calendar entry
=======================

In any :term:`data window` that shows calendar entries, you can click the
|insert| button in the :term:`toolbar` or hit the :kbd:`Insert` key to create a
new :term:`calendar entry`. Fill out the fields of the :term:`insert window` and
hit the :guilabel:`Create` button to submit the window.


Configure calendar functionality
==================================

See :doc:`/plugins/cal`.
