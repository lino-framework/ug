.. include:: /../docs/shared/include/defs.rst

.. _ug.howto.siteconfig:

==============================
Configure site-wide parameters
==============================

.. _ug.site_company:

How to specify the site operator
================================

The :term:`site manager` should create an :term:`organization` that represents
the :term:`site operator`, and then specify this in the :term:`Site parameters`.

Lino uses this information e.g. when printing your address in documents or
reports.  Or newly created partners inherit the country of the site operator.


- Select :menuselection:`Contacts --> Organizations` and create an organization
  that represents the :term:`site operator`.

- Open :menuselection:`Configuration --> System --> Site parameters`

- Find the field :attr:`lino.modlib.system.SiteConfig.site_company` and make
  sure it points to the :term:`site operator`
