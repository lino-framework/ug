.. include:: /../docs/shared/include/defs.rst

====================
Manage user accounts
====================

.. _ug.howto.users:

How to create a new user account
================================

- You need to be :term:`site manager` to have the permission to see and manage
  the list of user accounts on your :term:`Lino site`.

- Select :menuselection:`Configure --> System --> Users` in the :term:`main
  menu`

- Click the |insert| button in the :term:`toolbar` or hit the
  :kbd:`Insert` key.

- Lino opens the following :term:`insert window`:

    | **New User**
    | Username: ____    e-mail address: ____
    | First name: ____  Last name: _____
    | Language: _____   User type: _____

- Fill out the fields of the :term:`insert window` and hit the
  :guilabel:`Create` button to submit the window.


.. _ug.howto.password:

How to change your password
===========================

- Click on the :guilabel:`My settings` :term:`quick link` in the :term:`main
  page`, or select :term:`My settings` from the :term:`user menu`.

  Lino will show the :term:`My settings` window of your :term:`user account`.

- Click on the :guilabel:`*` button in the :term:`toolbar`.

- Lino opens the following :term:`dialog window`:

    | **Change password**
    | Current password: ____
    | New password: ____
    | New password again: ____

- Enter your current password, then (twice) your new password.

- Hit :kbd:`ENTER` or click the :guilabel:`OK` button.

Notes:

- Changing your password does not cause your session to sign out. You
  will need your new password only when Lino asks you to sign in
  again.  Depending on the site's configuration that might happen only
  when you signed out explicitly.

- A :term:`site manager` can invoke that action on any user, and they
  can leave the :guilabel:`Current password` field empty.



See also
========

- :doc:`/plugins/users`
