.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.payment_orders:

==============
Payment orders
==============

.. contents::
   :depth: 1
   :local:

Register a payment order
=========================

- In the :guilabel:`Journals` table of your :term:`dashboard`, click on the
  |insert| button behind the :term:`journal` where you want to add your
  :term:`payment order`.

- Alternatively, select :menuselection:`Accounting --> Accounting --> XYZ
  Payment Orders` in the :term:`main menu` and then hit the :kbd:`Insert` key or
  click the |insert| button in the :term:`toolbar`.

- Lino opens the following :term:`insert window`:

    | **New Payment Order**
    | Entry date: ____
    | Narration: ____

- Fill out the fields of the :term:`insert window` and hit :kbd:`Ctrl+S` or
  click the :guilabel:`Create` button to submit the window.

- Click on :guilabel:`Suggestions` in the :attr:`Workflow
  <lino.core.model.Model.workflow_buttons>` field in the upper area. Lino opens a
  :term:`data window` with :term:`booking suggestions <booking suggestion>`,
  i.e. movements that are potential candidates for payment.

  Select one or several movements. Click the :guilabel:`Fill` button (|flash|)
  in the :term:`toolbar`. Lino closes the  :term:`data window` with
  :term:`booking suggestions <booking suggestion>` and returns to the
  :term:`detail window` with your payment order. The selected bookings have been
  added.

- Alternatively (if you prefer to manually select suggestions for individual
  partners) press :kbd:`F2` in the :guilabel:`Partner` cell of the
  :term:`phantom row` and select a partner. If the partner has only one open
  movement, Lino will fill the payment for this movement. If the partner has
  multiple open movements, click on :guilabel:`Suggestions` in the
  :attr:`Workflow <lino.core.model.Model.workflow_buttons>` cell next to the
  partner and proceed as in the previous step.

- Register your :term:`payment order` by hitting :kbd:`Ctrl+X` or by clicking on
  :guilabel:`Registered` in the :guilabel:`Workflow` field. See also
  :ref:`ug.plugins.accounting.registering`.


Configure payment orders
========================

You must create one payment order journal for each bank account on which you
plan to use payment orders.

- Select :menuselection:`Configure --> Ledger --> Journals` in the :term:`main
  menu` to open the :term:`data window` of your journals.

- To configure an existing journal, double-click on the journal to open its
  :term:`detail window`.

- To create a new payment order journal, hit the :kbd:`Insert` key or
  click the |insert| button in the :term:`toolbar`.

- In the :term:`detail window` check the following fields:

  - Organization: the bank institution that is to execute orders in this journal.
  - Account: should be "(43xx) Pending payment orders"
  - Trade type: should be "Bank payment orders"
  - Voucher type: Payment Order (:class:`lino_xl.lib.finan.PaymentOrdersByJournal`)
