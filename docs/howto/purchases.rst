.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.purchases:

==========
Purchases
==========

.. contents::
   :depth: 1
   :local:


In a purchase invoice you usually just enter the provider and the total amount
of the invoice, and Lino will assist you for filling invoice items based on this
amount.


Register a purchase invoice
===========================

- In the :guilabel:`Journals` table of your :term:`dashboard`, click on the
  |insert| button behind the :term:`journal` where you want to add your
  :term:`purchase invoice`.

- Alternatively, select :menuselection:`Accounting --> Purchases --> Purchase
  invoices` in the :term:`main menu` and then  click the |insert| button in the
  :term:`toolbar` or hit the :kbd:`Insert` key.

- Lino opens the following :term:`insert window`:

    | **New invoice**
    | Partner: ____
    | Entry date: ____  Total to pay: ____

- Fill out the fields of the :term:`insert window` and hit :kbd:`Ctrl+S` or
  click the :guilabel:`Create` button to submit the window.

- Lino displays a :term:`detail window` with the newly created invoice.
  See :ref:`ug.plugins.accounting.purchase` for details about individual fields.

- Lino automatically created and filled out a first invoice item using the
  provider's :guilabel:`Purchase account` field, the :term:`VAT regime` and the
  amount in the :guilabel:`Total to pay` field.

- If everything is correct, register your invoice by hitting :kbd:`Ctrl+X` or by
  clicking on :guilabel:`Registered` in the :guilabel:`Workflow` field. See also
  :ref:`ug.plugins.accounting.registering`.

- If the partner's :guilabel:`Purchase account` and/or :term:`VAT regime` field
  are not set correctly, click on the symbol next to the partner name to open
  the partner's detail window and edit these fields so that the default
  behaviour works for the next invoice from that provider.



Partially declarable costs
==========================

.. currentmodule:: lino_xl.lib.vat

Example : An electricity invoice of 94,88 €.  Only 35% of the total amount is
declarable.  The whole invoice amount is to be paid to the provider, but we will
reclaim 65% of it back from a third-party partner, e.g. an employee.

- Manually enter 94.88 in :attr:`VatProductInvoice.total_incl`. Lino fills one
  invoice item. The general account of this item is either the provider's
  :attr:`purchase_account` or (if that field is empty)
  :attr:`lino_xl.lib.accounting.CommonAccounts.waiting`.

- Change the amount of the invoice item (:attr:`total_incl
  <InvoiceItem.total_incl>`) from 94.88 to 33.22 (94.88 * 0.35).
  Lino automatically sets :attr:`total_base
  <InvoiceItem.total_base>` to 27.68 € (33.22 / 1.20) and :attr:`total_vat
  <InvoiceItem.total_vat>` to 5.54 (33.22 - 27.68).

- Add a second item and manually set :attr:`InvoiceItem.account` to
  :attr:`undeclared_costs <lino_xl.lib.accounting.CommonAccounts.undeclared_costs>`.
  Lino automatically fills the remaining amount (94.88 - 33.22 = 61.66) into the
  :attr:`InvoiceItem.total_incl` field and computes the other amounts of the
  item. Lino fills the amounts of the item to those needed to reach the
  invoice's total. Since the :attr:`vat_class` field of :attr:`undeclared_costs
  <lino_xl.lib.accounting.CommonAccounts.undeclared_costs>` is :attr:`exempt
  <lino_xl.lib.vat.VatClasses.exempt>`, the item will have no VAT.

Partially deducible VAT
=======================

Example : An electricity invoice of 120,00 € (100 € base + 20€ VAT).
The whole invoice is to be paid to the provider and the costs are
fully booked as operational costs, but only 50% *of the VAT* is deducible.

- Manually enter 120 in :attr:`VatProductInvoice.total_incl`. Lino fills one
  invoice item. The general account of this item is either the provider's
  :attr:`purchase_account` or (if that field is empty)
  :attr:`lino_xl.lib.accounting.CommonAccounts.waiting`.

- Change the amount of the invoice item (:attr:`total_incl
  <InvoiceItem.total_incl>`) from 120 to 60 (120 * 0.5).
  Lino automatically sets :attr:`total_base
  <InvoiceItem.total_base>` to 50 € (60 / 1.20) and :attr:`total_vat
  <InvoiceItem.total_vat>` to 10 € (50 * 0.20).

- Add a second item and manually set :attr:`InvoiceItem.account` to the same
  account as in the first item. Lino fills the amounts of the item to those
  needed to reach the invoice's total.

- Now change the :attr:`vat_class` field to :attr:`exempt
  <lino_xl.lib.vat.VatClasses.exempt>`, which will re-compute the amounts of the
  item.



Manually editing the VAT amount of invoice items
================================================

End users can manually edit any amount of an invoice item.

When you enter a :attr:`total_incl <InvoiceItem.total_incl>`, Lino automatically
computes the :attr:`total_base <InvoiceItem.total_base>`  and :attr:`total_vat
<InvoiceItem.total_vat>`. But when entering data from a legacy system, you may
want to manually specify a different VAT amount.
