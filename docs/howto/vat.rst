.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.vat:

================
VAT declarations
================

.. contents::
   :depth: 1
   :local:

Configure your VAT declarations journal
=======================================

- Select :menuselection:`Configure --> Ledger --> Journals` in the :term:`main
  menu` to open the :term:`data window` of your journals.

- Double-click on the journal named "VAT declarations" to open its :term:`detail
  window`.  If there is no such journal, create it.

- Check the following fields:

  - Partner: the national :term:`tax office` that will receive declarations in
    this journal. The difference between due and deductible VAT of every
    :term:`VAT declaration` will be booked as a debit or credit against this
    partner.

  - Voucher type: should be "XXX VAT declarations" (where XXX is your
    :term:`national VAT module`)

  - Account: should be "(4513) VAT declared"

  - Trade type: should be "Taxes"


Register a VAT declaration
==========================

- In the :guilabel:`Journals` table of your :term:`dashboard`, click on the
  |insert| button behind the :term:`journal` where you want to add your
  :term:`VAT declaration`.

- Alternatively, select :menuselection:`Accounting --> VAT --> Belgian VAT
  declarations` in the :term:`main menu` and then  click the |insert| button in
  the :term:`toolbar` or hit the :kbd:`Insert` key.

- Lino opens the following :term:`insert window`:

    | **New Belgian VAT declaration**
    | Start period: ____
    | End period: ____

- Fill out the fields of the :term:`insert window`. Leave the :guilabel:`End period`
  empty when it is the same as the :guilabel:`Start period`.

- Hit :kbd:`Ctrl+S` or click the :guilabel:`Create` button to submit the
  :term:`insert window`.

- Note the "Movements" panel, which says "No data to display".

- Hit :kbd:`Ctrl+X` to register the declaration.

- Note the "Movements" panel, which now shows
  the :term:`ledger movements <ledger movement>` booked by this declaration.

- Switch to the :guilabel:`Values` tab to see the detailed for every field of
  your declaration.

- Use your :term:`tax office`'s normal procedure to introduce your declaration.

Using multiple VAT journals
===========================

You may ask your :term:`server administrator` to install an additional
:term:`national VAT modules <national VAT module>` and then have
one VAT journal for every national :term:`tax office`.
