.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.products:

====================
Manage your products
====================

.. contents::
   :depth: 1
   :local:

Manage the catalogue of your products
=====================================
