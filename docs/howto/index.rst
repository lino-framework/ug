.. include:: /../docs/shared/include/defs.rst
.. _ug.howto:

===========
Howto pages
===========

Pages with quick instructions about how to do something.

.. toctree::
   :maxdepth: 2

   contacts
   sales
   products
   calendar
   purchases
   accounting
   payment_orders
   vat
   siteconfig
   users
   upload
   images
