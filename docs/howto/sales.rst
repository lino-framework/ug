.. include:: /../docs/shared/include/defs.rst
.. _ug.howto.sales:

==========
Sales
==========

.. contents::
   :depth: 1
   :local:


Register a sales invoice
========================

- In the :guilabel:`Journals` table of your :term:`dashboard`, click on the
  |insert| button behind the :term:`journal` where you want to add your
  :term:`purchase invoice`.

- Alternatively, select :menuselection:`Sales --> Sales
  invoices` in the :term:`main menu` and then  click the |insert| button in the
  :term:`toolbar` or hit the :kbd:`Insert` key.

- Lino opens the following :term:`insert window`:

    | **New invoice**
    | Partner: ____
    |

- Fill out the fields of the :term:`insert window` and hit :kbd:`Ctrl+S` or
  click the :guilabel:`Create` button to submit the window.

- Fill the items of your invoice.

- Register your invoice by hitting :kbd:`Ctrl+X` or by clicking on
  :guilabel:`Registered` in the :guilabel:`Workflow` field.
  See also :ref:`ug.plugins.accounting.registering`.
