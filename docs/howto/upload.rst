.. include:: /../docs/shared/include/defs.rst

.. _ug.howto.upload:

============================
Upload files and manage them
============================

- Select :menuselection:`Office --> My Upload files` in the main menu.

.. image:: uploads_MyUploads.png
  :height: 10em

- Click the |insert| button in the toolbar or hit :kbd:`Insert` to add a new upload

- In the :guilabel:`File` field, click on "Choose" to choose a file.

- Hit the "Create" button

See also
========

- :doc:`images`
