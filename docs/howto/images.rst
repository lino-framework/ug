.. include:: /../docs/shared/include/defs.rst

.. _ug.howto.images:

===========================
Insert images into comments
===========================

- Upload the image.

- Select :menuselection:`Office --> My Upload files` in the main menu.


.. image:: uploads_MyUploads.png
  :height: 10em

- Click on the yellow |copy| button next to the image you want to insert.
  This will copy some text to your clipboard.

- Edit your comment. Hit :kbd:`Ctrl+V` to paste the content of your clipboard
  into the text of your comment.

Remarks
=======

The copied text inserted by :kbd:`Ctrl+V` is a :term:`memo command` and should
look like this::

    [file 123]

Where 123 is the identifying number of your image. Lino assigns a unique number
to each uploaded file.

See also
========

- :doc:`/memo/index`
- :doc:`/memo/upload`
