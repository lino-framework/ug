.. include:: /../docs/shared/include/defs.rst

.. _ug.basics:

===========
Lino basics
===========

.. contents::
   :depth: 1
   :local:


Signing in
==========

Most Lino sites require you to **sign in** using a username and password.

.. glossary::

  user settings

    The settings stored for your user account. You can edit your user settings
    by selecting :menuselection:`My settings` from the :term:`user menu`.  Some
    application also provide a :term:`quick link`.

  user menu

    The sub-menu behind your name in the upper right corner of the Lino window.
    It holds commands related to authentication:
    :term:`Sign out`,
    edit your user settings,
    :term:`My settings`.
    act as another user, ...


The main page
=============

When you enter a :term:`Lino site`, you see the :term:`main page`. The main page
includes the :term:`main menu`, some :term:`welcome messages <welcome message>`,
a series of :term:`quick links <quick link>` and your :term:`dashboard`.

Welcome messages get generated dynamically each time your main window is being
displayed. Unlike notifications you don't get rid of them by marking them as
seen.

These :term:`welcome messages <welcome message>` and :term:`quick links <quick
link>` are determined programmatically, you cannot "configure" them yourself
manually. If you think that a welcome message or quick link is missing or
superfluous, you should report this to your :term:`application developer`.

Each :term:`dashboard item` displays some data from your database.  Most Lino
applications allow you to configure that dashboard in your :term:`user
settings`.


.. glossary::

  main page

    The content of the browser page presented to the :term:`end user` before
    they select any action from the :term:`main menu`.

    See `The main page`_

  main menu

    The top-level menu that leads to all functions of an application. You can
    access it in every Lino window.  Each time you select a command from the main
    menu, Lino opens a new window which will be placed over any other open
    windows. A window remains open until you close it.  So you have a stack of
    windows.

  quick link

    A shortcut link in the :term:`main page` that opens some :term:`data window`
    or executes some other action.

  welcome message

    A short message in the :term:`main page` that inform the :term:`end user`
    about something after signing in.

  dashboard

    The series of :term:`data tables <data table>` displayed in the :term:`main
    page`.


Window types
============

.. glossary::

  data window

    A window that displays some data from the database. The layout and behaviour
    of a data window are defined by its :term:`data table`.

    See `Data windows`_.

  insert window

    A window that will cause a new :term:`database row` to be created when you
    submit it. It asks you to to enter some data fields of the new row.

  dialog window

    A pop-up window used to enter additional information before actually
    executing a requested action.

  parameter window

    A window used to edit the values of the parameters of an action.

..
  An :term:`insert window` can be called from both a :term:`detail window` and a
  :term:`grid window`.

.. _ug.basics.data_windows:

Data windows
============

Most commands of the main menu open a :term:`data window`, which displays some
set of data rows from your database. In most cases these data rows are presented
as a :term:`grid window`. And in most cases you can double-click or hit
:kbd:`RETURN` on a row in order to change the :term:`grid window` into a
:term:`detail window`.

.. glossary::

  grid window

    A :term:`data window` in :term:`grid mode`. It shows the set of data rows in
    a tabular layout with rows and columns, like a spreadsheet.

  detail window

    A :term:`data window` in :term:`detail mode`. It shows a single data row at
    a time  in order to focus on that :term:`database row` and to show more
    details about it.


- Hit :kbd:`Enter` or *double click* on a row of a grid window to open a
  :term:`detail window` on that row.

- Hit :kbd:`Escape` or click the X in the upper right corner of the
  window to close that window and return to the home screen.

- You can edit individual cells of a grid by hitting :kbd:`F2` or by simply
  clicking on it.

- The layout of a :term:`detail window` is defined by a :term:`detail layout`.
  Some tables don't have a :term:`detail layout` defined and hence they are
  always shown in :term:`grid mode`.

"grid" and "detail" are the most common :term:`display modes <display mode>` of
a :term:`data window`.


The toolbar
===========

Most data windows have a :term:`toolbar`, with a :term:`quick search field` and
a series of buttons for navigating or running actions.

.. image:: toolbar.png
  :width: 90%

.. glossary::

  toolbar

    A row with action buttons at the top of a :term:`data window`.

  navigation buttons

    A series of action buttons used to navigate within a set of data rows, i.e.
    to go  to the previous, next, first or last row within the set of rows
    defined by a :term:`data window`.

  action button

    Any button of a :term:`toolbar` that will fire an action when you click it.



The quick search field
======================

.. glossary::

  quick search field

    A field where you can enter text for quickly filtering the rows to be
    displayed.

If the search string starts with "#", then Lino searches for a row
with that :term:`primary key`.

If the search string starts with "\*", then Lino searches for a row
with that *reference*.

You might wonder how Lino knows where to search when you type some text in the
:term:`quick search field`.

For example, when doing a quick search in a list of :term:`persons <person>`,
Lino searches only the :attr:`name <lino_xl.lib.contacts.Partner.name>` field
and not for example the street. That's because street names often contain names
of persons. A user who enters "berg" in the quick search field don't want to see
all persons living in a street named after a person whose name contained "berg".

A special type of quick search is when the search string starts with "#".  In
that case you get the :term:`database row` with that :term:`primary key`.

.. _ug.basics.display_modes:

Display modes
=============

A :term:`data table` can have different *modes* to display the data. We call
these modes :term:`display mode`.

.. glossary::

  display mode

    The mode or layout used by a :term:`data table` to display its data.


Lino knows the following display modes:

.. grid:: 1 2 2 2

 .. grid-item::

  .. glossary::

    grid mode

      A :term:`display mode` that shows data as an editable table with rows and
      columns like a spreadsheet.

 .. grid-item::

  .. image:: albums.AllFiles.grid.png
    :width: 100%


 .. grid-item::

  .. glossary::

    detail mode

      A :term:`display mode` that shows data with one page for each row.

 .. grid-item::

  .. image:: albums.AllFiles.detail.png
    :width: 100%

 .. grid-item::

  .. glossary::

    list mode

      A :term:`display mode` that shows data as a list of items.

 .. grid-item::

  .. image:: albums.AllFiles.list.png
    :width: 100%

 .. grid-item::

  .. glossary::

    cards mode

      A :term:`display mode` that shows data as "cards".

 .. grid-item::

  .. image:: albums.AllFiles.cards.png
    :width: 100%

 .. grid-item::

  .. glossary::

    gallery mode

      A :term:`display mode` that shows its data as an image gallery.

 .. grid-item::

  .. image:: albums.AllFiles.gallery.png
    :width: 100%

.. glossary::

  summary mode

    A :term:`display mode` that shows a customizable summary of its data.

  plain mode

    A :term:`display mode` that shows its data as a "plain" html
    table that is not editable.


Not all data tables feature all display modes. It is up to the application
developer to specify which display modes are available in a given data table and
which of them is the default.

The :term:`grid mode` and the :term:`plain mode` are "tabular" because they use
"rows" and "columns".


Slave panels
============

In a :term:`detail window` you can have :term:`slave panels <slave panel>`.

For example, the :mod:`lino_xl.lib.contacts` plugins differentiates between
:term:`organizations <organization>` and :term:`persons <person>`, each of them
has its own :term:`database model`.  A third :term:`database model` defines the
relation between persons and organizations.

Here is the :term:`detail window` of an :term:`organization`, which shows the
persons who work for this organization (the bottom right panel,
:guilabel:`Contact persons of Bäckerei Ausdemwald`):

.. figure:: contacts.Companies.detail.png
  :width: 90%

We call such a panel a :term:`slave panel` (and its :term:`data table` a
:term:`slave table`) because they make sense only when the :term:`master
instance` is known. In our example, the organization *Bäckerei Ausdemwald* is
the :term:`master instance`. Don't take this politically.

A slave panel has a special button |eject| in its upper right corner. Click this
button to open that :term:`slave table` in a :term:`data window` on its own.
This is good to know for several reasons:

- If the slave panel's display mode is ``'summary'``, the |eject| button is the
  only way to see that data as a table.

- The slave panel is meant as a preview, it has no navigation toolbar and shows
  only a limited number of rows.


..
  For example if you have two :term:`database models <database model>`
  :class:`City` and :class:`Person`, with a :term:`foreign key`
  :attr:`Person.city` pointing to :class:`City`, then the :term:`application
  developer` might define a slave table :class:`PersonsByCity`, which displays
  only the persons who live in a given city. A slave table needs to know a master
  instance. In our example it would make no sense to ask for
  :class:`PersonsByCity` without specifying a city.

.. rubric:: Keep in mind

.. glossary::

  slave table

    A :term:`data table` that is designed to display only the rows that are
    *related* to a given :term:`database row`, which the slave table calls its
    :term:`master instance`.

  slave panel

    A panel showing data that is related to the current row being displayed in a
    :term:`detail window` but stored in a separate :term:`database table`. The
    actor behind a :term:`slave panel` is called a :term:`slave table`.

  master instance

    The database object that acts as master of a :term:`slave table`.

  master table

    A :term:`data table` that is not bound to a :term:`master instance`.



Site parameters
===============

Many Lino applications have a menu command :menuselection:`Configure --> System
--> Site parameters`, usually available only to :term:`site managers <site
manager>`.


Lino jargon
===========

Here is a list of the Lino jargon words that you should understand after having
read this page.

.. glossary::

  phantom row

    The last, empty, row in a :term:`grid window`. When you enter something into
    a cell on this row, Lino will create a new :term:`database row`.   You can
    double-click on the phantom row to open an :term:`insert window` and create
    a new item. Lino adds a phantom row only in a :term:`grid window` where you
    have permission to add new rows.

  choicelist

    A list of concepts that look like database rows but cannot be edited via the
    web interface. If you want to edit a choicelist, you need to ask your
    :term:`server administrator`. Such a change is usually trivial, but requires at
    least a server restart.

  simple text representation

    A single-line plain text that represents a given :term:`database row`
    independently of any context.

    The :term:`application developer` can customize this by overriding the
    :meth:`__str__ <lino.core.model.Model.__str__>` method of the database
    model.


.. _ug.front_ends:

About front ends
================

A :term:`Lino application` can run under two different :term:`front ends <front
end>`. The same "soul" can have different "skins".

.. glossary::

  ExtJS front end

    The classical :term:`front end` for Lino, based on the Sencha ExtJS
    JavaScript library.

  React front end

    The modern :term:`front end` for Lino, based on ReactJS and the
    PrimeReact widget library.

Both front ends are used in production.
The :term:`ExtJS front end` is older and won't change very much any more.
For new Lino sites we recommend the :term:`React front end`.

You may opt to use both front ends, in which case each front end has its own URL
so that you can easily switch between them. For example you might prefer the
:term:`React front end` when accessing Lino from your mobile phone while you use
the :term:`ExtJS front end` when working from your desktop computer. The `Demo
sites <https://www.lino-framework.org/demos.html>`__ page has an example of  a
Lino site with both front ends.

Lino actually provides more front ends than these two, but the other front ends
are experimental proofs of concept. The Developer Guide contains :ref:`more
about them <dev.front_ends>`.

Developer documentation for the the :term:`React front end` is at
https://react.lino-framework.org/index.html
