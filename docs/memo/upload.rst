.. _ug.memo.upload:

==============================================
``upload`` -- Insert an image or download link
==============================================

.. command:: [file]

The :cmd:`[file]` memo command inserts the specified :term:`upload file` into
the memo text, either as an image or as a download link.
It is defined by the :ref:`uploads <ug.plugins.uploads>` plugin.

The minimal form specifies just the :term:`primary key` of the upload file::

  [file 123]

It will use a default image format and the upload file's :guilabel:`Description`
as the "caption" (the text that appears when the user hovers with their mouse
over the image).

You can specify your own caption by adding it between the primary key and the
closing square bracket ("]")::

  [file 123 My caption]

You can specify a **format** by putting it *before* the caption, separated with
a pipe character ("|")::

  [file 123 wide|My caption]

Available format names are:

- ``thumb`` (default) : height 30ex and float right
- ``tiny`` : height 15ex and float right
- ``wide`` : width 100%
- ``solo`` : width 90%
- ``duo`` : width 45%
- ``trio`` : width 30%

If you want to specify a format but no caption, then you must still specify a
pipe character::

  [file 123 wide|]

Because without the "|" at the end, Lino would understand that "wide" is your
caption.

The formats ``thumb`` and ``tiny`` can have an additional option ``left``, which
must come *before* the format name, separated with another pipe character
("|")::

  [file 123 left|thumb|My caption]

To summarize, we can imagine that Lino counts the pipe characters:

- no pipe character means just a caption
- one pipe characters means a format and a caption
- two pipe characters means a format option, a format name and a caption
