========================
``url`` -- Insert a link
========================

Defined by :ref:`ug.plugins.memo`.


Examples:

- [url https://using.lino-framework.org]

- The [url https://using.lino-framework.org Lino User Guide]
