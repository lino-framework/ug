.. _ug.memo.gallery:

==============================================
``gallery`` -- Insert a series of pictures
==============================================

.. command:: [gallery]

The :cmd:`[gallery]` memo command inserts a number of :term:`upload files
<upload file>` as a single, centered, paragraph into the memo text.


For example::

  [gallery 1 2 3]

is equivalent to writing ``[file 1] [file 2] [file 3]`` inside a separate
centered paragraph.

Defined by :ref:`ug.plugins.uploads`.
