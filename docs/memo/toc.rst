.. _ug.memo.toc:

==============================================
``toc`` -- Insert a table of content
==============================================

.. command:: [toc]

The :cmd:`[toc]` memo command inserts a local table of content that lists all
child pages of a page (:ref:`ug.plugins.publisher`).
