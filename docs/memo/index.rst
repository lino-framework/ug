.. include:: /../docs/shared/include/defs.rst
.. _ug.memo:

=============
Memo commands
=============

Many text fields in Lino can contain :term:`memo commands <memo command>`.

.. glossary::

  memo command

    A fragment of text between square brackets that will be replaced by some
    other text when your text is being rendered at certain places.

For technical specs see :mod:`lino.modlib.memo`.

.. toctree::
   :maxdepth: 1

   upload
   url
   gallery
   toc
