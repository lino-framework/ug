.. _ug.topics.docs:

============================
About the Lino documentation
============================

The **Lino documentation** is distributed over several :term:`documentation
trees <documentation tree>`, each of them targeted at different audiences.

.. glossary::

  main website

    A :term:`doctree` for "everybody", including (future)
    :term:`site operators <site operator>`, stakeholders and consultants.

    https://www.lino-framework.org

  user guide

    A :term:`doctree` for :term:`end users <end user>`,
    :term:`key users <key user>` and :term:`support providers <support
    provider>`

    https://using.lino-framework.org

  hosting guide

    A :term:`doctree` for
    :term:`hosting providers <hosting provider>`

    https://hosting.lino-framework.org

  developer guide

    A :term:`doctree` for :term:`application developers
    <application developer>` and :term:`core developers <core developer>`.

    https://dev.lino-framework.org


The :term:`source code` of these documentation trees is published in :term:`code
repositories <code repository>` as free content under the same licence as the
source code of Lino itself. You can find the source code by clicking on the
"Source code" in the footer of every web page.

For some applications we maintain also :term:`end-user documentation` in
different languages. These are independent :term:`doctrees <doctree>`, not a
translation of the technical docs.

There is a whole section about :ref:`Writing documentation <writedocs>` in the
:term:`developer guide`.
