=====================================
How to clear your browser's site data
=====================================

When the software on the server of your Lino site has been updated, it can
happen that you need to clear your browser's **site data**. Here is how to do
this.

Note that removing your browser's site data never affects the database on the
server.  It is just what your browser remembers from your activity in previous
Lino sessions.  You will have to sign in again afterwards (re-enter your
password).

Firefox
=======

Click on the :kbd:`🔒` symbol in your address toolbar,

.. image:: clear_site_data_firefox.png
  :width: 50%

In this menu, click on "Clear cookies and site data". The browser then asks you:

  Removing cookies and site data may log you out of websites.  Are you sure you
  want to remove cookies and site data for **cosi1e.lino-framework.org**?

Click the "Remove" button to confirm the question.

Chromium
========

Click on the "View site information" button in your address toolbar, then select
"Cookies and site data". Chromium then shows the following window:

.. image:: clear_site_data_chromium.png
  :width: 50%

Click on the :kbd:`🗑` symbol to clear your site data.


..
  When the Javascript console says::

  Uncaught (in promise) TypeError: layout is undefined
      createMenu App.jsx:619
      fetch_site_data App.jsx:524
  App.jsx:619:21

​
