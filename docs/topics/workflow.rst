.. include:: /../docs/shared/include/defs.rst

.. _ug.ref.topics.workflow:

===============
About workflows
===============


Concepts
========

.. glossary::

  workflow state

    One of the possible states in the "life cycle" of a database object.
