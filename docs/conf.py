# -*- coding: utf-8 -*-
# fmt: off
import datetime

from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(globals())

# General substitutions.
project = "Lino User Guide"
html_title = "Lino User Guide"
copyright = '2019-{} Rumma & Ko Ltd'.format(datetime.date.today().year)

extensions += ['lino.sphinxcontrib.logo']

html_context.update({
    'display_gitlab': True,
    'gitlab_user': 'lino-framework',
    'gitlab_repo': 'ug',
})


# from rstgen.sphinxconf import interproject
#
# interproject.configure(globals(), "cg hg book")
# intersphinx_mapping['cg'] = ('https://community.lino-framework.org/', None)
# intersphinx_mapping['ug'] = ('https://using.lino-framework.org/', None)
# intersphinx_mapping['hg'] = ('https://hosting.lino-framework.org/', None)

html_use_index = True

if html_theme == "insipid":
    html_theme_options['right_buttons'].insert(0, 'languages-button.html')

# print("20221203", html_static_path, html_css_files)

rst_prolog = """

:doc:`Basics </basics/index>` | :doc:`Plugins </plugins/index>` |
:doc:`Howtos </howto/index>` | :doc:`Applications </apps/index>`

"""
