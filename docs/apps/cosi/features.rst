.. include:: /shared/include/defs.rst

.. _ug.cosi.features:

========================
Lino Così features
========================

With Lino Così you will finally perceive it as a joy to

- manage your **contacts**, **products** and **account chart**
- write and print your **sales invoices**

and maybe you will soon also use it to

- record your **purchase invoices**
- declare your **VAT statements**
- record your **bank statements**
- get your **financial situation**
