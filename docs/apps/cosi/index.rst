.. include:: /shared/include/defs.rst

.. _ug.cosi:

========================
Lino Così User Guide
========================

Welcome to the :term:`end-user documentation` about :ref:`cosi`.

Lino Così is a simple accounting application. It keeps things simple, but it is
extensible by design. Its codebase is used by more sophisticated applications
like :ref:`tera` or :ref:`voga`.

We pronounce it as the Italian word *così*, which sounds as "kozee" would sound
when read by a native English speaker. The name *Così* is both a fragment of the
question  "Comment faire une **co**\ mptabilité **si**\ mple?" (French for "How
to make accounting easy?") and our answer to that question: "Così!" (Italian for
"Like this!"). Lino Così is how we love accounting!


.. toctree::
   :maxdepth: 1

   features
   tour/index
