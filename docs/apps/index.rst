.. _ug.apps:

============
Applications
============

This section contains the user guides for :term:`privileged applications`.
Independent applications have their own end-user documentation.

.. toctree::
   :maxdepth: 1

   cosi/index
   cms/index
   noi/index
