.. include:: /shared/include/defs.rst

.. _ug.cms.features:

========================
Lino CMS features
========================

Lino CMS is a Content Management System à la Lino. It is experimental. It
doesn't even try to do things the way you are used to to them.

Once upon a time (in October 2023) I complained to a friend for whom I maintain
a small website XYZ:

- Me: Hey, you organize an interesting event,
  but I hear about it *accidentally* via an unrelated mailing list! Why didn't
  you ask me to publish it on XYZ?

- Friend: But I did invite you on Facebook.

- Me: (after checking) Oops it seems that I need to delete my FB account
  because it makes people think I am actually reading my notifications.

- Friend: I might have published the event on ABC (the website of my employer)
  and then post it on all social media platforms and send email notifications to
  everybody... but that's more work than creating it on FB and selecting the
  people to invite.

- Me: And this ignores the minority of people who refuse to use Facebook.

- Friend: Yes, I am sorry for them, but my workload is at a maximum.

- Me: What you need is a platform where you can create your invitations to
  events as easily as on Facebook and have them automatically published on
  multiple other platforms.

- Friend: That would be a great tool!

- Me: This is what we are trying to do with Lino CMS. It is one of the
  basic ideas of the `Fediverse <https://en.wikipedia.org/wiki/Fediverse>`__.
