.. include:: /shared/include/defs.rst

.. _ug.cms.tour:

============================
A guided tour into Lino CMS
============================

Let's have a look at our public demo site https://cms1.mylino.net

Click on the `Admin` link in the upper right corner to enter the web interface
where you can manage the content.

- :ref:`ug.basics`

Lino CMS actually just integrates the following standard plugins:

- :ref:`ug.plugins.blogs`
- :ref:`ug.plugins.uploads`
- :ref:`ug.plugins.publisher`
- :ref:`ug.plugins.comments`


Contacts
========

Lino CMS has a contacts management. Keep in mind that these are the contacts you
want to publish on your website.

See :ref:`ug.plugins.contacts`.

Products
========

Lino Cosi has a simple product management.

You might write templates to produce a printable catalog or a website
from these product.
