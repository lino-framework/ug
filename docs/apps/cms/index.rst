.. include:: /shared/include/defs.rst

.. _ug.cms:

========================
Lino CMS User Guide
========================

Welcome to the :term:`end-user documentation` about :ref:`cms`.

.. toctree::
   :maxdepth: 1

   features
   tour/index
