.. include:: /shared/include/defs.rst

.. _ug.noi.features:

========================
Lino Noi features
========================

Lino Noi is a customizable ticket management and worktime tracking system. It is
used to

- create tickets and triage them,
- discuss about tickets,
- record our working time
- generate service reports for our customers
- manage the :term:`time credit` of our customers
- generate yearly or monthly invoices for service level agreements,

Lino Noi is an integral part of the `Lino framework
<https://www.lino-framework.org>`__, a sustainably free open-source project
maintained by the `Synodalsoft team <https://www.synodalsoft.net>`__ sponsored
by `Rumma & Ko OÜ <https://www.saffre-rumma.net>`__. Your contributions are
welcome.
