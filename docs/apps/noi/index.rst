.. include:: /shared/include/defs.rst

.. _ug.noi:

========================
Lino Noi User Guide
========================

Welcome to the :term:`end-user documentation` about :ref:`noi`.

Lino Noi is the issue management and worktime tracking system used by the
`Synodalsoft team <https://www.synodalsoft.net/>`__. With Lino Noi we create our
:term:`tickets <ticket>`, triage them, discuss about them, we record our working
time and Lino generates service reports for the customers who wish this. Lino
also generates yearly or monthly invoices and manages the :term:`time credit`
for every customer.

Lino Noi is very unknown, compared to tools like Jira or GitHub.  After
evaluating a lot of existing software products we decided to `eat our own dog
food <https://en.wikipedia.org/wiki/Eating_your_own_dog_food>`__ and write a
:term:`Lino application`. We started this in April 2015 and until now we didn't
regret it. Lino Noi has become our primary team communication tool. We would
love to share our know-how and the result of ten years of experience with other
teams and help them to get started with Lino Noi.

.. toctree::
   :maxdepth: 1

   tour
   features
