from atelier.invlib import setup_from_tasks

cfg = dict()
cfg.update(revision_control_system='git')
# cfg.update(tolerate_sphinx_warnings=True)
cfg.update(blog_root='/home/luc/work/blog/')
cfg.update(languages=['en', 'de', 'fr'])
cfg.update(selectable_languages=('en','de'))
# cfg.update(doc_trees=['docs', 'dedocs'])
cfg.update(make_docs_command='./make_docs.sh')
cfg.update(intersphinx_urls=dict(docs="https://using.lino-framework.org"))
ns = setup_from_tasks(globals(), **cfg)
